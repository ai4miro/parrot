import shutil
import subprocess
from api_server.api import bp
from flask import jsonify, request
from api_server.api.errors import bad_request, error_response
import os
import json, uuid
import sys

import zipfile
import re
import requests
from bs4 import BeautifulSoup

# Define file paths and temporary directory
filepath = "python_environments/enviroments.json"
backupfilepath = filepath + ".bak"
backupfilepath2 = filepath + ".bak2"
temp_dir = 'api_server/tempDownloads/'

# Function to save environment information in JSON format
def save_env_info(data):
    print('Saving data in JSON')

    try:
        existing_data = []

        # Load existing data if the file exists
        if os.path.isfile(filepath):
            with open(filepath, "r") as infile:
                existing_data = json.load(infile)

        # Append new data to the existing data
        existing_data.append(json.loads(data))

        # Create backups of the old file
        backupfilepath = filepath + ".bak"
        backupfilepath2 = filepath + ".bak2"
        if os.path.isfile(filepath):
            # Check if the first backup file exists and replace it with the second backup file
            if os.path.isfile(backupfilepath):
                # If the second backup file exists, remove it
                if os.path.isfile(backupfilepath2):
                    os.remove(backupfilepath2)
                # Rename the first backup file to the second backup file
                os.replace(backupfilepath, backupfilepath2)
            # Rename the original file to the first backup file
            os.replace(filepath, backupfilepath)

        # Write the merged data back to the file
        with open(filepath, "w") as outfile:
            json.dump(existing_data, outfile, indent=2)

        return {"message": 'Saving environment information was successful'}
    except Exception as e:
        raise e


# API route to load environment information
@bp.route('/env/load', methods=['POST'])
def loadEnvInfo():
    if os.path.isfile(filepath):
        path = filepath
    elif os.path.isfile(backupfilepath):
        path = backupfilepath
    else:
        return jsonify([{
            "vId": "Error",
            "path": "Error",
            "name": "Error"
        }])
    try:
        with open(path, "r") as f:
            return f.read()
    except Exception as e:
        return error_response(500, "cannot read environment")


# Function to get Python versions from python.org
def get_python_versions():
    url = 'https://www.python.org/ftp/python/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    version_elements = soup.find_all('a', href=re.compile(
        r'^([3-9]|[1-9][0-9]+)\.\d+(\.\d+)?\/$'))
    versions = [element['href'] for element in version_elements]
    print('there are', len(versions), 'versions')
    return versions


# API route to get suitable Python versions for embedding
@bp.route('/env/get', methods=['POST'])
def get_suitable_versions():
    versions = get_python_versions()
    url = 'https://www.python.org/ftp/python/'
    all_embedded_links = []
    for version in versions:
        try:
            response = requests.get(url + version)
            response.raise_for_status()
            soup = BeautifulSoup(response.text, 'html.parser')
            embedded_links = soup.find_all('a', href=re.compile(r'^python-(\d+(\.\d+)*)?-embed-amd64\.zip$'))
            for link in embedded_links:
                version_number = re.search(r'python-(\d+\.\d+\.\d+)', link.text)
                link_info = {
                    'id': link.text,
                    'name': link.text,
                    'description': f'Python "{link.text[:-4]}" Embedded Distribution',
                    'author': 'Python Software Foundation',
                    'link': f'{url}{version}{link.text}',
                    'version': version_number.group(1)
                }
                all_embedded_links.append(link_info)
        except requests.RequestException as e:
            print(f"Error obtaining {version}: {e}")

    print(len(all_embedded_links))
    try:
        return all_embedded_links
    except Exception as e:
        return error_response(500, "cannot read environment")


# Function to download an environment
def download_environment(url, path):
    # Printing a message indicating that the download process has started
    print('Downloading...')
    # Sending a GET request to the provided URL with streaming enabled
    response = requests.get(url, stream=True)
    # Checking for any HTTP errors in the response; this will raise an exception if an error occurs
    response.raise_for_status()

    # Writing the response content to a file in binary mode
    with open(path, 'wb') as file:
        # Iterating over the response content in chunks and writing them to the file
        for chunk in response.iter_content(chunk_size=1024):
            file.write(chunk)
    # Printing a message indicating that the file has been successfully downloaded
    print(f"File downloaded successfully: {path}")


# API route to download and install a specific Python version
@bp.route('/env/download', methods=['POST'])
def download_install_version():
    # Extracting required data from the request
    link = request.json.get('link')
    name = request.json.get('name')
    version = request.json.get('version')

    try:
        # Generating environment and path names
        envNamePath = f'prediction-{version}'
        environment_path = f'python_environments/{envNamePath}'

        # Checking if the version already exists
        print('check if exists')
        if os.path.exists(environment_path):
            return jsonify({'message': 'This version already exists'}), 400

        # Creating temporary directory if it doesn't exist
        print('tempDirectory')
        if not os.path.exists(temp_dir):
            print(os.makedirs(temp_dir, mode=0o700))

        # Setting appropriate permissions for the temporary directory
        os.chmod(temp_dir, 0o700)

        # Downloading the environment
        print('calls downloads')
        download_environment(link, temp_dir + name)

        # Unzipping the downloaded environment
        print('unzip')
        with zipfile.ZipFile(temp_dir + name, 'r') as zip_ref:
            # Checking if the environment folder exists, creating it if not
            print('check if exists and/or makes new folder')
            if not os.path.exists(environment_path):
                os.makedirs(environment_path)
                os.chmod(temp_dir, 0o700)
            else:
                return jsonify({'message': 'This version already exists'}), 400
            # Extracting the contents of the ZIP file to the environment folder
            print('extracts')
            zip_ref.extractall(environment_path)
            # Generating JSON data to write to file
            version_without_dots = version.replace(".", "")
            print('builds json to write to file')
            new_environment = {
                "vId": f'py{version_without_dots}',
                "path": envNamePath,
                "name": f'Python{version}'
            }
            # Writing environment information to file
            print('write to environments')
            save_env_info(json.dumps(new_environment))

            # Installing pip within the environment
            get_pip = 'api_server/pythonUtils/get-pip.py'
            subprocess.Popen(
                [environment_path + '/python.exe', get_pip],
                shell=False,
            )
            zip_ref.close()
            print('deleting temp folder ')
            # Deleting the temporary directory
            shutil.rmtree(temp_dir)
            # Sending success response
            response = jsonify({
                'message': 'Saving environment information was successful'
            })
            print('successful end :) ')

            response.status_code = 200
            return response

    except Exception as e:
        # Handling errors
        return jsonify({'error': 'Storing the environment information failed: ' + str(e)}), 500


# API route to uninstall a specific Python version
@bp.route('/env/uninstall', methods=['POST'])
def uninstallEnv():
    try:
        envs_path = 'python_environments/'
        version = request.json.get('version')

        # Load existing data if the file exists
        if os.path.isfile(filepath):
            with open(filepath, "r") as infile:
                existing_data = json.load(infile)
                print("Existing data loaded successfully.")
        else:
            existing_data = []
            print("No existing data found. Creating a new list.")

        # Create a new list excluding the environment with the specified version
        updated_data = []
        for env in existing_data:
            if version not in env['name']:
                updated_data.append(env)
            else:
                print(env['name'])
                envs_path += env['path'] + '/'
        print(f"Filtered out environment with version {version}.")
        print(envs_path)
        # Write the updated data back to the file
        with open(filepath, "w") as outfile:
            json.dump(updated_data, outfile, indent=2)
            print("Updated data written to file successfully.")

        # deletes the python environment
        shutil.rmtree(envs_path)
        print('environments deleted')

        response = jsonify({
            'message': 'Environment uninstallation was successful'
        })
        response.status_code = 200
        return response

    except Exception as e:
        print(f"An error occurred: {str(e)}")
        response = jsonify({
            'error': 'Failed to uninstall the environment'
        })
        response.status_code = 500
        return response
