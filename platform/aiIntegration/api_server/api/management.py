from api_server.api import bp
from api_server.api.errors import bad_request, error_response
from flask import jsonify, request
import os
import pathlib

# POST
#   {
#       path: "a/b/c"   or   ["a","b","c"]
#   }
# Result:
#   {
#       absolutepath: "the absolutepath"
#       pathparts: ["a","b","c"]
#       dirnames: [ list of directory names ],
#       filenames: [ list of filenames ]
#       pathsep: "/" or "\"
#   }
# If path is a file, the directory of the file is used.
# Error: 404 if path does not exist


@bp.route('/dir', methods=['POST'])
def dir():
    data = request.get_json() or {}
    if ('path' not in data) or data["path"] == "":
        return bad_request("Path is empty")

    path = data["path"]
    if type(path) is list:
        path = os.path.join("", *path)

    # Get directory path if path is a file
    if not os.path.isdir(path):
        path = os.path.dirname(path)

    path = os.path.realpath(path)

    dirpath, dirnames, filenames = next(
        os.walk(path), (None, None, []))
    if dirpath:
        return jsonify({
            "absolutepath": path,
            "pathparts": list(pathlib.Path(path).parts),
            "dirnames": dirnames,
            "filenames": filenames,
            "pathsep": os.sep
        })
    else:
        return error_response(404, "cannot read provided path")


# POST
#   {
#       path: "a/b/c"   or   ["a","b","c"]
#   }
# Result:
#   {
#       path: "a/b/c",
#       content: "content of file"
#   }
# Error: 404 if path does not exist or if path is a directory

@bp.route('/file', methods=['POST'])
def file():
    data = request.get_json() or {}
    if ('path' not in data) or data["path"] == "":
        return bad_request()

    path = data["path"]
    if type(path) is list:
        path = os.path.join("", *path)

    if os.path.isdir(path):
        return error_response(404, "path is not a file")
    else:
        try:
            with open(path, "r") as f:
                return jsonify({
                    "path": path,
                    "content": f.read(),
                })
        except Exception as e:
            return error_response(404, "cannot read path")