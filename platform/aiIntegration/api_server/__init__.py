from flask import Flask
import ctypes
import platform

if platform.system()=="Windows":
    ctypes.windll.user32.ShowWindow(ctypes.windll.kernel32.GetConsoleWindow(), 0)

# def create_ai-integration():
api_server = Flask(__name__)

from api_server.api import bp as api_bp  # noqa
api_server.register_blueprint(api_bp, url_prefix='/api')
