import * as DICOMWeb from './index.js';

describe('Top level exports', () => {
  test('should export the modules getAttribute, getModalities, getName, getNumber, getString', () => {
    const expectedExports = [
      'getAttribute',
      'getModalities',
      'getName',
      'getNumber',
      'getString',
    ].sort();

    const exports = Object.keys(DICOMWeb.default).sort();

    expect(exports).toEqual(expectedExports);
  });
});
