
import Cookies from 'universal-cookie';

const settingValue = '/dicom-web'

const SETTING_NAMES = {
  PACS_URL: 'PACS_URL',
};

class MainSettings {
  constructor() {
    this.cookies = new Cookies();
    this.cookies.set(SETTING_NAMES.PACS_URL, settingValue, { path: '/' });
  }

  // set(settingName, settingValue) {
  //   this.cookies.set(settingName, settingValue, { path: '/' });
  // }
  
  get(settingName) {
    return (this.cookies.get(settingName));
  }
}

// const SETTING_NAMES = {
//   PACS_URL: 'PACS_URL',
// };

export {SETTING_NAMES, MainSettings};

