import React from 'react';
import presets from '../presets.js';
import './ModalStyle.css';
import { Icon } from '../../../utils/icons/Icon';

export default class LayoutSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() {}

  render() {
    return (
      <div className="layoutOptions" id="layoutOptions">
        <Icon className={this.props.layout == 3 ? 'medIcon' + ' active' : 'medIcon'} name="layoutThree" onClick={() => this.props.setLayout(3)} />
        <div className="space20px"></div>
        <Icon className={this.props.layout == 2 ? 'medIcon' + ' active' : 'medIcon'} name="layoutTwo" onClick={() => this.props.setLayout(2)} />
      </div>
    );
  }
}
