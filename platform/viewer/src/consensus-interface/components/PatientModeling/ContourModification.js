export default class ContourModification {
  static instance = null;
  onModicationStartFunc = [];
  onModicationEndFunc = [];
  activeSeg = null;
  segs = null;

  static getInstance(){
    if (!ContourModification.instance)
      ContourModification.instance = new ContourModification();
      
    return this.instance;
  }
  
  cancel() {
    this.activeSeg = null;
    this.onModicationEndFunc.forEach(func => func());
  }
  
  // setActiveSeg to null will save the modifications
  setActiveSeg(segNb){
    switch(segNb) {
      case null:
        if(!this.activeSeg)
          return;

        this.activeSeg = null;    
        this.onModicationEndFunc.forEach(func => func());
        break;
      default:
        this.activeSeg = segNb,
        this.onModicationStartFunc.forEach(func => func());
    }
  }
  
  onModicationStart(func) {
    this.onModicationStartFunc.push(func);
  }
  
  onModicationEnd(func) {
    this.onModicationEndFunc.push(func);
  }
  
  removeModificationStartListener(listener) {
    const listenerIndex = this.onModicationStartFunc.indexOf(listener);
    
    if (listenerIndex>-1)
      this.onModicationStartFunc.splice(listenerIndex, 1)
  }
  
  removeModificationEndListener(listener) {
    const listenerIndex = this.onModicationEndFunc.indexOf(listener);
    
    if (listenerIndex>-1)
      this.onModicationEndFunc.splice(listenerIndex, 1)
  }
}

