const presets = [
  {
    name: 'CT-Bone (450, 1600)',
    windowWidth: '1600',
    windowLevel: '450',
    gradientOpacity: '4 0 1 985.12 1',
    specularPower: '1',
    scalarOpacity: '8 -1000 0 152.19 0 278.93 0.190476 952 0.3 3071 1',
    id: 'CT-Bone',
    specular: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '20 -1000 0.3 0.3 1 -488 0.3 1 0.3 463.28 0.3 0.3 0.3 659.15 0.7 0.7 0.7 953 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '152.19 952',
  },
  {
    name: 'CT-Brain (35, 100)',
    windowWidth: '100',
    windowLevel: '35',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '1',
    scalarOpacity: '10 -2048 0 -167.01 0 -160 0.2 240 0.7 3661 1',
    id: 'CT-Brain',
    specular: '0',
    shade: '0',
    ambient: '0.2',
    colorTransfer:
      '20 -2048 0 0 0 -167.01 0 0 0 -160 0.0556356 0.0556356 0.0556356 240 1 1 1 3661 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '-167.01 240',
  },
  {
    name: 'CT-Dental (400, 2000)',
    windowWidth: '2000',
    windowLevel: '400',
    gradientOpacity: '4 0 1 985.12 1',
    specularPower: '1',
    scalarOpacity: '8 -1000 0 152.19 0 278.93 0.190476 952 0.3 3071 1',
    id: 'CT-Dental',
    specular: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '20 -1000 0.3 0.3 1 -488 0.3 1 0.3 463.28 0.3 0.3 0.3 659.15 0.7 0.7 0.7 953 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '152.19 952',
  },
  {
    name: 'CT-Larynx (40, 250)',
    windowWidth: '250',
    windowLevel: '40',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '10',
    scalarOpacity:
      '10 -3024 0 67.0106 0 251.105 0.446429 439.291 0.625 3071 0.616071',
    id: 'CT-Larynx',
    specular: '0.2',
    shade: '1',
    ambient: '0.1',
    colorTransfer:
      '20 -3024 0 0 0 67.0106 0.54902 0.25098 0.14902 251.105 0.882353 0.603922 0.290196 439.291 1 0.937033 0.954531 3071 0.827451 0.658824 1',
    selectable: 'true',
    diffuse: '0.9',
    interpolation: '1',
    effectiveRange: '67.0106 439.291',
  },
  {
    name: 'CT-Liver (50, 350)',
    windowWidth: '350',
    windowLevel: '50',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '10',
    scalarOpacity:
      '10 -3024 0 67.0106 0 251.105 0.446429 439.291 0.625 3071 0.616071',
    id: 'CT-Liver',
    specular: '0.2',
    shade: '1',
    ambient: '0.1',
    colorTransfer:
      '20 -3024 0 0 0 67.0106 0.54902 0.25098 0.14902 251.105 0.882353 0.603922 0.290196 439.291 1 0.937033 0.954531 3071 0.827451 0.658824 1',
    selectable: 'true',
    diffuse: '0.9',
    interpolation: '1',
    effectiveRange: '67.0106 439.291',
  },
  {
    name: 'CT-Lung (-600, 1600)',
    windowWidth: '1600',
    windowLevel: '-600',
    gradientOpacity: '6 0 1 985.12 1 988 1',
    specularPower: '1',
    scalarOpacity: '12 -1000 0 -600 0 -599 0.15 -400 0.15 -399 0 2952 0',
    id: 'CT-Lung',
    specular: '0',
    references: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '24 -1000 0.3 0.3 1 -600 0 0 1 -530 0.134704 0.781726 0.0724558 -460 0.929244 1 0.109473 -400 0.888889 0.254949 0.0240258 2952 1 0.3 0.3',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '-600 -399',
  },
  {
    name: 'CT-Mediastinum (40, 400)',
    windowWidth: '400',
    windowLevel: '40',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '10',
    scalarOpacity:
      '10 -3024 0 67.0106 0 251.105 0.446429 439.291 0.625 3071 0.616071',
    id: 'CT-Mediastinum',
    specular: '0.2',
    shade: '1',
    ambient: '0.1',
    colorTransfer:
      '20 -3024 0 0 0 67.0106 0.54902 0.25098 0.14902 251.105 0.882353 0.603922 0.290196 439.291 1 0.937033 0.954531 3071 0.827451 0.658824 1',
    selectable: 'true',
    diffuse: '0.9',
    interpolation: '1',
    effectiveRange: '67.0106 439.291',
  },
  {
    name: 'CT-Pelvis (250, 1000)',
    windowWidth: '1000',
    windowLevel: '250',
    gradientOpacity: '4 0 1 985.12 1',
    specularPower: '1',
    scalarOpacity: '8 -1000 0 152.19 0 278.93 0.190476 952 0.3 3071 1',
    id: 'CT-Pelvis',
    specular: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '20 -1000 0.3 0.3 1 -488 0.3 1 0.3 463.28 0.3 0.3 0.3 659.15 0.7 0.7 0.7 953 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '152.19 952',
  },
  {
    name: 'CT-Soft tissue (40, 350)',
    windowWidth: '350',
    windowLevel: '40',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '10',
    scalarOpacity:
      '10 -3024 0 67.0106 0 251.105 0.446429 439.291 0.625 3071 0.616071',
    id: 'CT-Soft tissue',
    specular: '0.2',
    shade: '1',
    ambient: '0.1',
    colorTransfer:
      '20 -3024 0 0 0 67.0106 0.54902 0.25098 0.14902 251.105 0.882353 0.603922 0.290196 439.291 1 0.937033 0.954531 3071 0.827451 0.658824 1',
    selectable: 'true',
    diffuse: '0.9',
    interpolation: '1',
    effectiveRange: '67.0106 439.291',
  },
  {
    name: 'CT-Spine (35, 300)',
    windowWidth: '300',
    windowLevel: '35',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '10',
    scalarOpacity:
      '10 -3024 0 67.0106 0 251.105 0.446429 439.291 0.625 3071 0.616071',
    id: 'CT-Spine',
    specular: '0.2',
    shade: '1',
    ambient: '0.1',
    colorTransfer:
      '20 -3024 0 0 0 67.0106 0.54902 0.25098 0.14902 251.105 0.882353 0.603922 0.290196 439.291 1 0.937033 0.954531 3071 0.827451 0.658824 1',
    selectable: 'true',
    diffuse: '0.9',
    interpolation: '1',
    effectiveRange: '67.0106 439.291',
  },
  {
    name: 'CT-Vertebrae (350, 2000)',
    windowWidth: '2000',
    windowLevel: '350',
    gradientOpacity: '4 0 1 985.12 1',
    specularPower: '1',
    scalarOpacity: '8 -1000 0 152.19 0 278.93 0.190476 952 0.3 3071 1',
    id: 'CT-Vertebrae',
    specular: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '20 -1000 0.3 0.3 1 -488 0.3 1 0.3 463.28 0.3 0.3 0.3 659.15 0.7 0.7 0.7 953 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '152.19 952',
  },
  {
    name: 'CONTOURS ONLY',
    windowWidth: '2',
    windowLevel: '0',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '1',
    scalarOpacity: '4 -3024 0 3071 0',
    id: 'CONTOURS ONLY',
    specular: '0',
    shade: '0',
    ambient: '1',
    colorTransfer: '8 -3024 0 0 0 3071 0 0 0',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '-1 1',
  },
  {
    name: 'MR-Default',
    windowWidth: '500',
    windowLevel: '0',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '1',
    scalarOpacity: '12 0 0 20 0 40 0.15 120 0.3 220 0.375 1024 0.5',
    id: 'MR-Default',
    specular: '0',
    shade: '1',
    ambient: '0.2',
    colorTransfer:
      '24 0 0 0 0 20 0.168627 0 0 40 0.403922 0.145098 0.0784314 120 0.780392 0.607843 0.380392 220 0.847059 0.835294 0.788235 1024 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '0 220',
  },
  {
    name: 'MR-MIP',
    windowWidth: '500',
    windowLevel: '0',
    gradientOpacity: '4 0 1 255 1',
    specularPower: '1',
    scalarOpacity: '8 0 0 98.3725 0 416.637 1 2800 1',
    id: 'MR-MIP',
    specular: '0',
    shade: '0',
    ambient: '0.2',
    colorTransfer: '16 0 1 1 1 98.3725 1 1 1 416.637 1 1 1 2800 1 1 1',
    selectable: 'true',
    diffuse: '1',
    interpolation: '1',
    effectiveRange: '0 416.637',
  },
];

export default presets;
