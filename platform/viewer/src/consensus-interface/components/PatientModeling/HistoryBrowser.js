import React, { useMemo } from 'react';
import ContourModification from './ContourModification.js';
import { useTable, useFilters, useSortBy } from 'react-table';
import { ColumnFilter } from '../../utils/Filters.js';
import { FiDelete } from 'react-icons/fi';
import { Modal } from 'antd';
import { confirmAlert } from 'react-confirm-alert';
import './styles.css';

/** Delete history table*/

class DeleteHistoryInfoDataTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'deleteHistory' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
          width: '50%',
        }}
      >
        {headerName}
      </th>
    );
  }
  renderTableTBody() {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'center',
    };
    return (
      <tbody className="deleteHistoryTableTB">
        <tr className="deleteHistoryTR">
          <td className="deleteHistryContourNameTD">{this.props.contourName}</td>
          <td className="deleteHistoryLabelTD">{this.props.labelText}</td>
        </tr>
      </tbody>
    );
  }

  render() {
    return (
      <table
        className="deleteHistoryInfoDataTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'center',
          borderCollapse: 'inherit',
          width: '100%',
        }}
      >
        <thead>
          <tr className="deleteHistoryInforDataTableTH">
            {this.renderTableHeader('Contour Name')}
            {this.renderTableHeader('Label')}
          </tr>
        </thead>
        {this.renderTableTBody()}
      </table>
    );
  }
}

/** ModificationHistoryTable */
const COLUMNS = [
  {
    Header: 'Label',
    Footer: 'Label',
    accessor: 'label',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Description',
    Footer: 'Description',
    accessor: 'description',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Contour Name',
    Footer: 'Contour Name',
    accessor: 'name',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Modification Time',
    Footer: 'Modification Time',
    accessor: 'time',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: '',
    Footer: '',
    accessor: 'delete',
    disableFilters: true,
    sticky: 'center',
  },
];

export const ModificationHistoryTable = ({ histories, counter, historySelection, onClickDeleteHistory, onClickShowSelectedHistory }) => {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => {
    const lh = histories.slice().filter((h) => historySelection[h.contourID] !== undefined);
    const allContourIDs = [...new Set(lh.map((h) => h.contourID))];

    lh.push(
      ...allContourIDs.map((contourID) => ({
        label: 'initial',
        description: 'initial',
        contourID: contourID,
        time: '-',
        delete: 'No',
        attachmentID: null,
      }))
    );
    return lh;
  }, [counter, histories, historySelection]);

  const defaultColumn = React.useMemo(
    () => ({
      Filter: ColumnFilter,
    }),
    []
  );

  const deleteSelectedHistory = (entry) => {
    onClickDeleteHistory(entry);
  };

  const showSelectedHistory = (entry) => {
    onClickShowSelectedHistory(entry);
  };

  const tableStyle = {
    border: 'sold 1px grey',
    fontSize: '14px',
    padding: '2px',
    textAlign: 'center',
    width: '100%',
    borderCollapse: 'inherit',
  };

  const headerStyle = {
    border: 'solid 1px black',
    textAlign: 'center',
    fontSize: '12px',
    padding: '2px',
    backgroundColor: '#151A1F',
    color: '#9CCEF9',
  };

  const tdStyle = {
    padding: '1px',
    border: 'solid 1px grey',
    fontSize: '14px',
    textAlign: 'center',
  };

  const { getTableProps, getTableBodyProps, headerGroups, footerGroups, rows, prepareRow, state } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: {
        sortBy: [
          {
            id: 'time',
            desc: true,
          },
        ],
      },
    },
    useFilters,
    useSortBy
  );

  return (
    <>
      <table className="contourModificationHistoryTable" style={tableStyle} {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr className="contourModificationHistoryHeaderTR" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())} style={headerStyle}>
                  <div className="contourModificationHistoryTHDiv">
                    {column.render('Header')}
                    <span>{column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : ''}</span>
                  </div>
                </th>
              ))}
            </tr>
          ))}
          {headerGroups.map((headerGroup) => (
            <tr className="contourModificationHistoryHeaderTR" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th style={headerStyle}>{column.id != 'delete' ? <td>{column.render('Filter')}</td> : <td></td>}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="cmHistoryTableTBody" {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr
                {...row.getRowProps()}
                className={historySelection[row.original.contourID].vtkID == row.original.attachmentID ? 'tableRowSelected' : 'noSelectedRow'}
              >
                {row.cells.map((cell) => {
                  if (cell.column.id != 'delete') {
                    return (
                      <td {...cell.getCellProps()} style={tdStyle} onClick={(event) => showSelectedHistory(row.original)}>
                        {cell.column.id === 'name' ? historySelection[row.original.contourID].name : cell.render('Cell')}
                      </td>
                    );
                  } else {
                    return row.original.delete == 'No' ? null : (
                      <td className="actionDeleteStudyTD">
                        <FiDelete
                          className={'deleteHistoryRowIcon'}
                          style={{
                            fontSize: '15px',
                          }}
                          onClick={(event) => deleteSelectedHistory(row.original)}
                        />
                      </td>
                    );
                  }
                })}
              </tr>
            );
          })}
        </tbody>
        <tfoot>
          {footerGroups.map((footerGroup) => {
            <tr {...footerGroup.getFooterGroupProps()}>
              {footerGroup.headers.map((column) => (
                <td {...column.getFooterProps()}>{column.render('Footer')}</td>
              ))}
            </tr>;
          })}
        </tfoot>
      </table>
    </>
  );
};

export default class HistoryBrowser extends React.Component {
  constructor(props) {
    super(props);

    this.contourModification = ContourModification.getInstance();
    /*
        if(!this.props.hasSegments) {
          this.state = {
            histories: [], 
            selectedEntryForDelete: null,
          }
          return;
        }
    */
    this.state = {
      histories: props.contourModificationHistoryDataSet.getHistoryInfoData(),
      selectedEntryForDelete: null,
      contourModificationHistoryDataSet: props.contourModificationHistoryDataSet,
      counter: 0,
    };

    this.endListener = () => {
      this.setState({
        histories: props.contourModificationHistoryDataSet.getHistoryInfoData(),
        counter: this.state.counter + 1,
      });
    };

    this.contourModification.onModicationEnd(this.endListener);
  }

  componentWillUnmount() {
    this.contourModification.removeModificationEndListener(this.endListener);
  }

  onOKConfirmedDeleteHistoryInfoData() {
    this.props
      .onDeleteHistory(this.state.selectedEntryForDelete)
      .catch(() => {
        confirmAlert({
          title: 'Delete Modification History',
          message: 'Delete modification history failed!',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {},
            },
          ],
        });
      })
      .finally(() => {
        this.setState({
          histories: this.props.contourModificationHistoryDataSet.getHistoryInfoData(),
          counter: this.state.counter + 1,
          selectedEntryForDelete: null,
        });
      });
  }

  renderContourModificationHistoryTable() {
    return this.state.histories.length ? (
      <div className="controlModificationHistoryTableDiv">
        <ModificationHistoryTable
          histories={this.state.histories}
          counter={this.state.counter}
          historySelection={this.props.historySelection}
          onClickDeleteHistory={(entry) => this.setState({ selectedEntryForDelete: entry })}
          onClickShowSelectedHistory={(entry) => this.props.onSelectHistory(entry)}
        />
      </div>
    ) : (
      <h3 style={{ color: 'red' }}>No contour modification history</h3>
    );
  }

  getDeleteConfirmModalContent() {
    if (this.state.selectedEntryForDelete) {
      return (
        <div className="deleteHistoryInfoDataTableDiv" style={{ width: 'fit-content' }}>
          <h3 className="confirmDeleteHistoryMessage" style={{ color: 'yellow', textAlign: 'center', fontSize: '25px' }}>
            Do you really want to delete this modification?
          </h3>
          <DeleteHistoryInfoDataTable
            contourName={this.props.historySelection[this.state.selectedEntryForDelete.contourID]?.name}
            labelText={this.state.selectedEntryForDelete.label}
          ></DeleteHistoryInfoDataTable>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="PatientModelingHistoryView">
        <div className="scrollHistoryList">{this.renderContourModificationHistoryTable()}</div>
        <Modal
          className="confirmDeleteHistoryDialog"
          closable={false}
          visible={this.state.selectedEntryForDelete != null}
          onCancel={() => this.setState({ selectedEntryForDelete: null })}
          onOk={() => this.onOKConfirmedDeleteHistoryInfoData()}
        >
          {this.getDeleteConfirmModalContent()}
        </Modal>
      </div>
    );
  }
}
