import { Button, Dropdown, Menu, Space } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';
import '../../../PatientModeling/PlanEvaluation/style/parameterSetting_styles.css';
import NTCPContourMappingDialog from '../../../PatientModeling/PlanEvaluation/DosePlanVisualization/NTCPContourMappingDialog';

const baselineScores = ['0 grade 0-I', '1 grade II', '2 grade III-IV'];
const locations = ['0=oral cavity', '1=pharynx', '2=larynx'];

export default class ParameterSetting extends React.Component {
  constructor(props) {
    super(props);

    ParameterSetting.propTypes = {
      ntcpModelName: PropTypes.string,
      ntcpSettings: PropTypes.array,
      segDataSet: PropTypes.object,
    };

    this.onBaselineValueChange = this.onBaselineValueChange.bind(this);
    this.onLocationChange = this.onLocationChange.bind(this);
    this.NTCPContourMappingDialogFunctions = {};

    const setting = props.ntcpSettings.find((s) => s.modelName === props.ntcpModelName)?.ntcpParameterSetting;
    if (props.ntcpModelName === 'ntcpLIPPv22') {
      this.state = {
        selectedBaselineScore: setting.baselineScore,
        selectedLocation: setting.location,
        ntcpContourMapping: setting.ntcpContourMapping,
        ntcpMappingErrorMessage: '',
      };
    } else if (props.ntcpModelName === 'ntcpEsophagusStricture') {
      this.state = {
        ntcpContourMapping: setting.ntcpContourMapping,
        ntcpMappingErrorMessage: '',
      };
    } else {
      this.state = { ntcpMappingErrorMessage: '' };
    }
  }

  onBaselineValueChange(newBaselineScoreValue) {
    this.setState({
      selectedBaselineScore: newBaselineScoreValue,
    });
    this.props.ntcpSettings.find((s) => s.modelName === this.props.ntcpModelName).ntcpParameterSetting.baselineScore = newBaselineScoreValue;
  }

  onLocationChange(newLocationValue) {
    this.setState({
      selectedLocation: newLocationValue,
    });
    this.props.ntcpSettings.find((s) => s.modelName === this.props.ntcpModelName).ntcpParameterSetting.location = newLocationValue;
  }

  onContourMappingChanged(newMapping) {
    this.setState({
      ntcpContourMapping: newMapping,
    });
    this.props.ntcpSettings.find((s) => s.modelName === this.props.ntcpModelName).ntcpParameterSetting.ntcpContourMapping = newMapping;
  }

  renderNtcpLippBaselineScoreDropdown() {
    const menu = (
      <div className="ntcpBaselineScoreDropdownDiv">
        <Menu
          items={baselineScores.map((item, index) => {
            return {
              key: index,
              label: <label style={{ color: 'black' }}>{item}</label>,
            };
          })}
          onClick={({ key }) => this.onBaselineValueChange(baselineScores[key])}
        />
      </div>
    );
    return (
      <Space direction="vertical">
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <Button
              style={{
                backgroundColor: 'white',
                color: 'black',
                border: '1px solid grey',
                borderRadius: '2px',
                width: '250px',
                marginLeft: '38px',
              }}
            >
              {this.state.selectedBaselineScore == null ? '---- no select ----' : this.state.selectedBaselineScore}
            </Button>
          </Dropdown>
        </Space>
      </Space>
    );
  }

  renderNtcpLippLocationDropdown() {
    const menu = (
      <div className="ntcpLippLocationDropdownDiv">
        <Menu
          items={locations.map((item, index) => {
            return {
              key: index,
              label: <label style={{ color: 'black' }}>{item}</label>,
            };
          })}
          onClick={({ key }) => this.onLocationChange(locations[key])}
        />
      </div>
    );
    return (
      <Space direction="vertical" style={{ padding: '10px' }}>
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <Button
              style={{
                backgroundColor: 'white',
                color: 'black',
                border: '1px solid grey',
                borderRadius: '2px',
                width: '250px',
                marginLeft: '27px',
              }}
            >
              {this.state.selectedLocation == null ? '---- no select ----' : this.state.selectedLocation}
            </Button>
          </Dropdown>
        </Space>
      </Space>
    );
  }

  renderNTCPLippV22ModelParameter() {
    return (
      <div>
        <h3 className="ntcpLippParamterLabel">Parameters:________________________________________</h3>
        <table className="ntcpLippParameterTable">
          <tbody className="ntcpLippParameterTableTB">
            <tr className="ntcpLippBaselineScoreTR">
              <td className="ntcpLippBaselineScoreLabelTD">Baseline Score: </td>
              <td className="ntcpLippBaselineScoreDropdownTd">{this.renderNtcpLippBaselineScoreDropdown()}</td>
            </tr>
            <tr className="ntcpLippLocationTR">
              <td className="ntcpLippLocationLabelTD">Location: </td>
              <td className="ntcpLippLocationDropdownTD">{this.renderNtcpLippLocationDropdown()}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  renderParametersByModel() {
    if (this.props.ntcpModelName === 'ntcpLIPPv22') {
      return (
        <Space direction="vertical">
          <div>{this.renderNTCPLippV22ModelParameter()}</div>
          <Button
            className="mappingContoursBtn"
            style={{ color: 'yellow', width: '350px', fontSize: '16px', height: '40px', marginTop: '15px' }}
            onClick={() => {
              this.NTCPContourMappingDialogFunctions.setVisible(true);
            }}
          >
            Match channels to available contours
          </Button>
        </Space>
      );
    } else if (this.props.ntcpModelName === 'ntcpEsophagusStricture') {
      return (
        <Button
          className="mappingContoursBtn"
          style={{ color: 'yellow', width: '350px', fontSize: '16px', height: '40px', marginTop: '15px' }}
          onClick={() => {
            this.NTCPContourMappingDialogFunctions.setVisible(true);
          }}
        >
          Match channels to available contours
        </Button>
      );
    } else {
      return <h2 style={{ color: 'red', marginTop: '5px' }}>Select an NTCP model</h2>;
    }
  }

  render() {
    return (
      <div>
        {this.renderParametersByModel()}
        <NTCPContourMappingDialog
          NTCPContourMappingDialogFunctions={this.NTCPContourMappingDialogFunctions}
          ntcpContourMapping={this.state.ntcpContourMapping}
          ntcpModelName={this.props.ntcpModelName}
          onMappingChanged={(newMapping) => {
            this.onContourMappingChanged(newMapping);
          }}
        />
      </div>
    );
  }
}
