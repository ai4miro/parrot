import React, { useEffect, useRef, useState } from 'react';
import Grid from '@mui/material/Grid';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader, ClickAwayListener,
  Popper,
  Typography,
} from '@mui/material';
import './styles.css';
import { Col, Container, Row } from 'react-bootstrap';

//-------------------------------------------------api------------------------------------------------------


// Define an asynchronous function named actionSend that takes two parameters: url and arg
async function actionSend(url, arg) {
  // Send a POST request to the specified URL with the arg parameter serialized as JSON
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(arg),
    // Set the request headers to indicate that the content type is JSON
    headers: {
      'Content-Type': 'application/json',
    },
  });
  // If the response status is 200 (OK)
  if (response.status == 200) {
    // Parse the response body as JSON
    const data = await response.json();
    // Log the data to the console
    console.log(data);
    // Return the parsed data
    return data;
  } else {
    // If the response status is not 200
    // Read the response body as text
    const text = await response.text();
    var error = text;
    // Try to parse the response body as JSON to extract the error message
    try {
      error = JSON.parse(text).message;
    } catch {
      // If parsing fails, keep the original error text
    }
    // Throw the error
    throw error;
  }
}


//-----------------------------------------content processing-----------------------------------------------


function selectWhatToDo(data, installed) {


  // Check if the link contains 'python'
  if (data.includes('python')) {
    // If 'python' is installed and the link contains 'python', return the uninstall route
    if (installed) {
      console.log('unninstall');
      return '/aimodels/env/uninstall';
    }
    // Otherwise, return the link
    return '/aimodels/env/download';
  }
  // Check if the link contains 'ai4miro' in any case
  else if (data.toLowerCase().includes('ai4miro')) {
    // If the link contains 'ai4miro', return the corresponding Hugging Face link

    const link = 'https://huggingface.co/' + data;
    return '/aimodels/modelinfo/download';
  }
}


//------------------------------------------frontend--------------------------------------------------------
function Products({ name, description, author, link, installed, version, onProductAction }) {

  // Define a state variable 'open' and a function 'setOpen' to toggle its value
  const [open, setOpen] = useState(false);

// Define a function 'handleToggle' to toggle the value of 'open'
  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

// Define a function 'handleClose' to set 'open' to false
  const handleClose = () => {
    setOpen(false);
  };

// Define an asynchronous function 'handleUninstall' to handle uninstallation
  const handleUninstall = async () => {
    // Log 'uninstall' to the console
    console.log('uninstall');
    // Create an argument object with a 'version' property
    const argument = { 'version': version };
    // Send a request to perform uninstallation
    await actionSend(selectWhatToDo(link, true), argument);
    // Call 'onProductAction' with the argument 'correct'
    onProductAction('correct');
  };

// Define an asynchronous function 'handleInstall' to handle installation
  const handleInstall = async () => {
    // Log 'install' to the console
    console.log('install');
    let args = [];
    // Check if the link includes 'ai4miro' in lowercase
    if (link.toLowerCase().includes('ai4miro')) {
      // If true, set 'args' object with a 'link' property pointing to a Hugging Face model
      args = { 'link': 'https://huggingface.co/' + description + '/' + name };

      // Alert the user about future feature and provide additional info
      alert('Future feature, working on it. \nFor more info in the model you want go to: \n \n'
        + 'https://huggingface.co/' + description + '/' + name+
        '\n \n'
      );

      // Return true
      return true;

      // The following lines will not be executed because 'return' exits the function
      console.log('entra');
      console.log(args);
    } else {
      // If the link does not include 'ai4miro', set 'args' object with 'link', 'name', and 'version' properties
      args = {
        'link': link,
        'name': name,
        'version': version,
      };
    }
    // Send a request to perform installation
    await actionSend(selectWhatToDo(link, false), args);
    // Call 'onProductAction' with the argument 'correct'
    onProductAction('correct');
  };


  return (
    <Grid item xs={4}>
      <Card>
        <CardHeader title={name}></CardHeader>
        <CardContent>
          {description ? (
            <Typography variant='body1' component='p' className='smalldescription'>
              {description}
            </Typography>
          ) : (
            <Typography variant='body1' component='p'>
              <br />
            </Typography>
          )}
          {author !== null ? (
            <Typography variant='body2' color='#bdb2b1'>
              Author: {author}
            </Typography>
          ) : (
            <Typography variant='body2' color='#bdb2b1'>
              Author: <br />
            </Typography>
          )}
        </CardContent>
        <CardActions>
          <Popper open={open} placement='top' transition>
            {({ TransitionProps }) => (
              <ClickAwayListener onClickAway={handleClose}>
                <Box
                  {...TransitionProps}
                  className='box'
                >
                  <Container>
                    <Row>
                      <Button onClick={handleClose} className='cardButton'> <Typography  color='#fff'>
                        X
                      </Typography></Button>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <div className='descriptionBox'>
                          <Typography variant='body2' color='#fff' sx={{ overflow: 'auto' }}>
                            {description} <br />
                          </Typography>
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </Box>
              </ClickAwayListener>
            )}
          </Popper>

          <Button aria-describedby={open ? 'product-description' : undefined} type='button' onClick={handleToggle}
                  className='cardButton'>
            <Typography color='#fff'>
              Learn more
            </Typography> </Button>
          {!installed ? (
            <Button onClick={handleInstall} className='cardButton'> <Typography color='#fff'>
              Install
            </Typography></Button>
          ) : (
            <Button onClick={handleUninstall} className='cardButton'> <Typography color='#fff'>
              Uninstall
            </Typography></Button>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default Products;









