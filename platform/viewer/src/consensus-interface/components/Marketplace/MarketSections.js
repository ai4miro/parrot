import React, { useEffect, useState } from 'react';
import { Input } from 'antd';
import Products from './Product';
import { Accordion, AccordionDetails, AccordionSummary, Box, Grid, LinearProgress } from '@mui/material';
import './styles.css';

function Section({ name, filteredModels, error, installed, onProductAction, loading }) {

  // Define a state variable 'searchItem' and a function 'setSearchItem' to manage the search input value
  const [searchItem, setSearchItem] = useState('');

  // Define a state variable 'filteredProducts' and a function 'setFilteredProducts' to manage the filtered products
  const [filteredProducts, setFilteredProducts] = useState(filteredModels);

  // Define a side effect that updates 'filteredProducts' when 'filteredModels' changes
  useEffect(() => {
    // Set 'filteredProducts' to initial 'filteredModels'
    setFilteredProducts(filteredModels);
    // Log 'filteredModels' to the console
    console.log(filteredModels);
  }, [filteredModels]);

  // Define a function 'handleSearchChange' to handle search input changes
  const handleSearchChange = (e) => {
    // Get the search value from the input and convert it to lowercase
    const searchValue = e.target.value.toLowerCase();
    // Set 'searchItem' to the lowercase search value
    setSearchItem(searchValue);

    // Filter 'filteredModels' based on search criteria and update 'filteredProducts'
    const filtered = filteredModels.filter(
      (product) =>
        product.name.toLowerCase().includes(searchValue) || // Check if the name includes the search value
        (product.description && product.description.toLowerCase().includes(searchValue)) || // Check if the description includes the search value (if available)
        (product.author && product.author.toLowerCase().includes(searchValue)), // Check if the author includes the search value (if available)
    );
    // Set 'filteredProducts' to the filtered result
    setFilteredProducts(filtered);
  };

  // If error renders this in the webpage
  if (error) {
    return <div>Error: {error}</div>;
  }

  // if (!loading) {
  //   return (<>
  //       <Accordion>
  //         <AccordionSummary className='accordion-summary'>
  //           {name}
  //         </AccordionSummary>
  //       </Accordion>
  //     </>
  //   );
  // }

  return (
    <Accordion radioGroup={3}>
      <AccordionSummary className='accordion-summary' >
        {name}
      </AccordionSummary>

      <AccordionDetails className='accordion-details'>
        <div>
          <Grid container>
            {!loading ? (
                <Box sx={{ paddingLeft: 5, width: '98%' }}>
                  <LinearProgress />
                </Box>
              ) :
              <Grid item xs={12} marginY={2}>
                <Input
                  className='input-prov'
                  type='text'
                  value={searchItem}
                  onChange={handleSearchChange}
                  placeholder={`Search your ${name}`} />
              </Grid>
            }
            <Grid item xs={12}>
              <Grid container spacing={1} justifyContent='center'>
                {filteredProducts.map(product => (
                  <Products
                    key={product.id}
                    name={product.name}
                    description={product.description}
                    author={product.author}
                    link={product.link}
                    version={product.version !== null ? product.version : undefined} // Conditionally pass version prop
                    installed={installed}
                    onProductAction={onProductAction}
                  />
                ))}
              </Grid>
            </Grid>
          </Grid>
        </div>
      </AccordionDetails>
    </Accordion>
  )
    ;
}

export default Section;
