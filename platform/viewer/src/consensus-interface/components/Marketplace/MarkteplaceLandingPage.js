import React, { useEffect, useState } from 'react';
import Section from './MarketSections';
import './styles.css';
import { Box, LinearProgress } from '@mui/material';

// https://i.pravatar.cc/300

//--------------------------------------------------api-----------------------------------------------------

// Function to retrieve AI model list from Hugging Face API
async function aiModelListRetrive() {
  // Fetch AI model list from Hugging Face API
  const response = await fetch(
    'https://huggingface.co/api/models?author=AI4MIRO&limit=5&full=true&config=true',
    {
      method: 'GET',
      headers: {}, // No additional headers required
    },
  );
  // If response status is OK (200)
  if (response.status == 200) {
    // Parse response body as JSON
    const data = await response.json();
    // Process the retrieved data from Hugging Face
    const processedData = processDataHugging(data);
    // Return the processed data
    return processedData;
  } else {
    // If response status is not OK
    // Get error message from response body
    const text = await response.text();
    var error = text;
    try {
      // Try to parse the error message as JSON and get the 'message' property
      error = JSON.parse(text).message;
    } catch {
      // If parsing fails, keep the original error text
    }
    // Throw the error
    throw error;
  }
}

// Function to retrieve information about installed AI models
async function aiModelsInstalled() {
  // Fetch installed AI models information from server
  const response = await fetch(
    '/aimodels/modelinfo/load',
    {
      method: 'POST',
      headers: {}, // No additional headers required
    },
  );
  // If response status is OK (200)
  if (response.status == 200) {
    // Parse response body as JSON
    const data = await response.json();
    // Return the parsed data
    return data;
  } else {
    // If response status is not OK
    // Get error message from response body
    const text = await response.text();
    var error = text;
    try {
      // Try to parse the error message as JSON and get the 'message' property
      error = JSON.parse(text).message;
    } catch {
      // If parsing fails, keep the original error text
    }
    // Throw the error
    throw error;
  }
}

// Function to retrieve information about Python versions
async function pythonVersions() {
  // Fetch Python versions information from server
  const response = await fetch(
    '/aimodels/env/get',
    {
      method: 'POST',
      body: '{}', // Empty JSON body
      headers: {
        'Content-Type': 'application/json', // Specify content type as JSON
      },
    },
  );
  // If response status is OK (200)
  if (response.status == 200) {
    // Parse response body as JSON
    const data = await response.json();
    // Log the retrieved data to the console
    console.log('proxypass');
    console.log(data);
    // Return the parsed data
    return data;
  } else {
    // If response status is not OK
    // Get error message from response body
    const text = await response.text();
    var error = text;
    try {
      // Try to parse the error message as JSON and get the 'message' property
      error = JSON.parse(text).message;
    } catch {
      // If parsing fails, keep the original error text
    }
    // Throw the error
    throw error;
  }
}

// Function to load environment information
async function loadEnvInformation() {
  // Fetch environment information from server
  const response = await fetch('/aimodels/env/load', {
    method: 'POST',
    body: '{}', // Empty JSON body
    headers: {
      'Content-Type': 'application/json', // Specify content type as JSON
    },
  });
  // If response status is OK (200)
  if (response.status == 200) {
    // Parse response body as JSON
    const data = await response.json();
    // Log the retrieved data to the console
    console.log(data);
    // Return the parsed data
    return data;
  } else {
    // If response status is not OK
    // Get error message from response body
    const text = await response.text();
    var error = text;
    try {
      // Try to parse the error message as JSON and get the 'message' property
      error = JSON.parse(text).message;
    } catch {
      // If parsing fails, keep the original error text
    }
    // Throw the error
    throw error;
  }
}


// ---------------------------------------Data Processing-----------------------------------------

async function compareEnvironments(installedPromise, availablePromise) {
  // Wait for both the installed and available arrays to be fetched
  const installed = await installedPromise;
  const available = await availablePromise;

  // Make a copy of the available objects array
  const objExcluded = available.map(obj => ({ ...obj }));
  // Make a copy of the installed objects array
  const objInstalled = installed.map(obj => ({ ...obj }));

  // Make a copy of objExcluded to work with
  const filteredObj = [...objExcluded];

  const installedObj = [];

  // Loop through installed objects
  for (const env of objInstalled) {
    // Get the lowercase name of the environment
    const name = env.name.toLowerCase();
    // Loop through the filtered objects
    for (const excluded of filteredObj) {
      // Check if the environment name includes the lowercase version of the excluded version
      if (name.includes(excluded.version.toLowerCase())) {
        // Remove the excluded object from the filtered array
        filteredObj.splice(filteredObj.indexOf(excluded), 1);
        installedObj.push(excluded);
      }
    }
  }

  // Log the lengths of the arrays
  console.log('Installed environments:', objInstalled.length);
  console.log('Available environments:', available.length);
  console.log('Excluded environments:', filteredObj.length);

  // Return the installed and filtered arrays
  return [installedObj.sort(), filteredObj.sort()];
}


// Function to process data retrieved from Hugging Face API
async function processDataHugging(inputData) {
  // Map over the input data array asynchronously
  const outputData = await inputData.map(item => ({
    // Extract and format relevant information from each item
    id: item._id,
    name: item.id.split('/')[1],
    description: item.id.split('/')[0],
    author: item.id.split('/')[0],
    link: item.id,
    version: null, // Version is set to null by default
  }));
  // Return the processed output data
  return outputData;
}

// --------------------------------------------------Front End---------------------------------------

function Market() {
  // Define state variables and their respective update functions
  const [aiModels, setAiModels] = useState([]);
  const [pythonEnv, setPythonEnv] = useState([]);
  const [installedEnv, setInstalledEnv] = useState([]);
  const [isLoading, setLoading] = useState(false);

// Use useEffect hook to fetch products when component mounts
  useEffect(() => {
    fetchProducts();
  }, []);

// Function to fetch products asynchronously
  const fetchProducts = async () => {
    try {
      // Fetch AI models list
      const models = await aiModelListRetrive();
      // Fetch installed AI models information
      const installedModels = await aiModelsInstalled();
      // Load environment information
      const installed = await loadEnvInformation();
      // Retrieve available Python versions
      const available = await pythonVersions();
      // Compare installed and available environments
      const comparedEnv = await compareEnvironments(installed, available);

      // Log installed AI models to the console
      console.log(installedModels);

      // Update state variables with fetched data
      setAiModels(models);
      setInstalledEnv(comparedEnv[0]);
      setPythonEnv(comparedEnv[1]);
      setLoading(true); // Set loading state to true after data is fetched
    } catch (error) {
      // Handle errors during data fetching
      console.error('Error fetching AI models:', error);
    } finally {
      // Any finalization logic can be added here if needed
    }
  };

// Function to handle product action change
  const productActionChange = () => {
    setLoading(false); // Set loading state to false
    fetchProducts(); // Fetch products again
    setLoading(true); // Set loading state to true
  };


  return (
    <div className='market-container'>
      <Section name='Python versions' filteredModels={pythonEnv} onProductAction={productActionChange}
               loading={isLoading}></Section>
      <Section name='AI Models' filteredModels={aiModels} onProductAction={productActionChange}
               loading={isLoading}></Section>
      <Section name='Installed Enviroments' filteredModels={installedEnv} installed={true}
               onProductAction={productActionChange} loading={isLoading}></Section>
      {/* Future section when the install models is completed */}
      {/*<Section name='Installed Models' filteredModels={installedEnv} installed={true}*/}
      {/*         onProductAction={productActionChange} loading={isLoading}></Section>*/}
    </div>
  );
}

export default Market;
