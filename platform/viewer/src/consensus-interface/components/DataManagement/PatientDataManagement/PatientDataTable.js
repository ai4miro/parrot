import React from 'react';
import { FiDelete } from 'react-icons/fi';

import './styles.css';

export function RenderTable({ patients, handleLoadClick, buttonText, setIsDeleting, setPatientDeletingID, activePatient }) {
  return (
    <>
      {
        // if there is no patients... we dont want show the table
        patients.length > 0 && (
          <table className="patientManagementTable">
            <thead>{renderTableHeader()}</thead>
            <tbody>{renderTableBody(patients, handleLoadClick, buttonText, setIsDeleting, setPatientDeletingID, activePatient)}</tbody>
          </table>
        )
      }
    </>
  );
}

const renderTableHeader = () => {
  return (
    <tr>
      <th className="patientManagementTableHeaderTD"></th>
      <th className="patientManagementTableHeaderTD">Patient ID</th>
      <th className="patientManagementTableHeaderTD">Patient Name</th>
      <th className="patientManagementTableHeaderTD">Birthday</th>
      <th className="patientManagementTableHeaderTD">Sex</th>
      <th className="patientManagementTableHeaderTD"></th>
    </tr>
  );
};

const renderTableBody = (patients, handleLoadClick, buttonText, setIsDeleting, setPatientDeletingID, activePatient) => {
  return patients.map((patient) => (
    <tr
      className={activePatient && activePatient.MainDicomTags.PatientID === patient.MainDicomTags.PatientID ? 'tableRowSelected' : 'noSelectedRow'}
      key={patient.MainDicomTags.PatientID}
    >
      <td
        className="patientManagementTableBodyTD loadCell"
        style={{ cursor: 'pointer' }}
        title="Loading patient from the backend orthanc server. A study can be loaded in the study management tab and manipulated in the other two modules"
        onClick={() => handleLoadClick(patient)}
      >
        {buttonText(patient)}
      </td>
      <td className="patientManagementTableBodyTD">{patient.MainDicomTags.PatientID}</td>
      <td className="patientManagementTableBodyTD">{patient.MainDicomTags.PatientName}</td>
      <td className="patientManagementTableBodyTD">{patient.MainDicomTags.PatientBirthdate}</td>
      <td className="patientManagementTableBodyTD">{patient.MainDicomTags.PatientSex}</td>
      <td
        className="patientManagementTableBodyTD deleteCell"
        onClick={() => {
          setPatientDeletingID(patient.ID);
          setIsDeleting(true);
        }}
      >
        <FiDelete value={patient.MainDicomTags.PatientName} title="Delete the selected patient from the application" />
      </td>
    </tr>
  ));
};
