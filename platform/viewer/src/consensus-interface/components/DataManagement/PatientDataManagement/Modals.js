import React from 'react';
import { Modal } from 'antd';

import './styles.css';

// Showed to confirm the removal of a patient
export function ModalDelete({ isDeleting, handleDeletePatient, patientDeletingID, setIsDeleting }) {
  return (
    <Modal
      closable={false}
      open={isDeleting}
      onOk={() => handleDeletePatient(patientDeletingID)}
      onCancel={() => setIsDeleting(false)}
      style={{ width: 'fit-content' }}
    >
      <h3 className="confirmDeletePatientlabel">Do you really want to delete this patient?</h3>
    </Modal>
  );
}

// Showed WHILE removing a patient
export function ModalDeleting({ confirmDelete }) {
  return (
    <Modal closable={false} open={confirmDelete} footer={null} style={{ width: 'fit-content' }}>
      <h3 className="deletingPatientlabel">Deleting Patient...</h3>
    </Modal>
  );
}

/*Error modal*/
export function ModalUploadActiveStudy({ isUploadingHistoryOfActiveStudy, setIsUploadingHistoryOfActiveStudy }) {
  <Modal
    closable={false}
    open={isUploadingHistoryOfActiveStudy}
    zIndex={1000}
    cancelButtonProps={{ style: { display: 'none' } }}
    onOk={() => {
      setIsUploadingHistoryOfActiveStudy(false);
    }}
  >
    <h3>
      The modification history that you are trying to upload belongs to the currently active study and has therefore been ignored. Uploading it would result in
      inconsistent modifications. Please reload the webpage to unload the currently active study and try again.
    </h3>
  </Modal>;
}

/*Error modal*/
export function ModalStudyHasHistory({ studyAlreadyHasHistory, setStudyAlreadyHasHistory }) {
  <Modal
    closable={false}
    open={studyAlreadyHasHistory}
    zIndex={1000}
    cancelButtonProps={{ style: { display: 'none' } }}
    onOk={() => {
      setStudyAlreadyHasHistory(false);
    }}
  >
    <h3>
      The modification history that you are trying to upload belongs to an existing study that already has modifications. To replace the modification history,
      either delete the existing modifications or delete and reupload the entire study.
    </h3>
  </Modal>;
}

/*Error modal*/
export function ModalUploadedHistoryFailed({ uploadedHistoryCreationFailed, setUploadedHistoryCreationFailed }) {
  return (
    <Modal
      closable={false}
      open={uploadedHistoryCreationFailed}
      zIndex={1000}
      cancelButtonProps={{ style: { display: 'none' } }}
      onOk={() => {
        setUploadedHistoryCreationFailed(false);
      }}
    >
      <h3>The modification history that you have uploaded could not be processed. Please verify that it belongs to one of the existing studies.</h3>
    </Modal>
  );
}
