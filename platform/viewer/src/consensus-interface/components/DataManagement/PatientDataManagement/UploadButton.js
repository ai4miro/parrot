import React from 'react';

import ContourModificationHistoryDataSet from './../../../data/ContourModificationHistoryDataSet';
import './styles.css';

var jsZip = require('jszip');

export function UploadButton({
  setUploadedFiles,
  setFilesToUpload,
  uploadDicomFileFunctions,
  activeStudyInstanceUID,
  setIsUploadingHistoryOfActiveStudy,
  setStudyAlreadyHasHistory,
  uploadedFiles,
  setUploadedHistoryCreationFailed,
}) {
  // Used when you upload a JSON file (history data)
  let uploadedHistoryData = null;
  // Used when you upload a ZIP file
  let totalNumberOfFilesToUpload = 0;

  const dataUploadHandler = (event) => {
    const files = event.target.files;
    if (files && files.length) {
      setUploadedFiles([]);
      setFilesToUpload(files.length);

      uploadDicomFileFunctions.setVisible(true);

      totalNumberOfFilesToUpload = 0;

      uploadedHistoryData = null;

      for (let i = 0; i < files.length; i++) {
        let fileName = files[i].name.trim();
        if (fileName.toUpperCase().endsWith('ZIP')) {
          uploadZipFileToServer(files[i]);
        } else {
          uploadSingleFileToServer(fileName, files[i]);
        }
      }
    }
  };

  // TODO: CHECK THIS FUNCTION
  const uploadZipFileToServer = (zipfile) => {
    console.log(`Uploading zip file ${zipfile.name}`);

    jsZip.loadAsync(zipfile).then(function (zip) {
      zip.forEach(function (filepath) {
        // upload the file
        const f = zip.file(filepath);
        // f is null if filepath is a directory
        if (f) {
          totalNumberOfFilesToUpload++; // uploadedFiles.length
          setFilesToUpload(totalNumberOfFilesToUpload);
          // upload the file content to the server
          f.async('blob').then(function (blob) {
            uploadSingleFileToServer(filepath, blob);
          });
        }
      });
      //totalNumberOfFilesToUpload--; // don't count the zip file
      //setFilesToUpload(totalNumberOfFilesToUpload);
    });
  };

  const uploadSingleFileToServer = (fileName, file) => {
    if (fileName.toUpperCase().endsWith('JSON')) {
      // file is probably a modification history file.
      file
        .text()
        .then((text) => {
          const historyData = JSON.parse(text);
          if (!historyData.StudyInstanceUID || !historyData.history) {
            throw Error('Invalid history file');
          } else if (historyData.StudyInstanceUID == activeStudyInstanceUID) {
            // Used to show a modal
            setIsUploadingHistoryOfActiveStudy(true);
            throw Error('Uploaded history belongs to active study');
          } else {
            const contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();
            contourModificationHistoryDataSet
              .loadDataAsync(historyData.StudyInstanceUID)
              .then(() => {
                if (contourModificationHistoryDataSet.getHistoryInfoData().length > 0) {
                  setStudyAlreadyHasHistory(true);
                  fileUploadFailed(fileName);
                } else {
                  uploadedHistoryData = historyData;
                  fileUploadSuccessfull(fileName);
                }
              })
              .catch((error) => {
                throw error;
              })
              .finally(() => {
                contourModificationHistoryDataSet.delete();
              });
          }
        })
        .catch((error) => {
          fileUploadFailed(fileName);
        });
    } else if (fileName.toUpperCase().endsWith('DCM')) {
      const formData = new FormData();
      formData.append('files[]', file);
      const requestOptions = {
        method: 'POST',
        headers: {},
        body: formData,
      };

      fetch('/uploaddicom', requestOptions)
        .then((res) => {
          if (res.ok) {
            fileUpload(fileName, true);
          } else {
            throw Error('upload failed');
          }
        })
        .catch((err) => {
          fileUpload(fileName, false);
        });
    } else {
      console.log('You haven`t uploaded a ZIP or a DICOM file' + fileName);
      fileUpload(fileName, false);
    }
  };

  const fileUpload = (fileName, successfullyUploaded) => {
    setUploadedFiles((prevFiles) => {
      const newList = [...prevFiles, { fileName: fileName, status: successfullyUploaded }];
      checkUploadFinished(newList.length);
      console.log(`uploaded ${fileName} ${successfullyUploaded ? 'successfully' : 'failed'}`);
      return newList;
    });
  };

  const checkUploadFinished = (numberOfUploadedFiles) => {
    if (numberOfUploadedFiles === uploadedFiles.length) {
      // all files uploaded.
      // we process the modification history file now
      if (uploadedHistoryData) {
        const contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();
        contourModificationHistoryDataSet
          .getOrthancInstanceID(uploadedHistoryData.StudyInstanceUID)
          .then((iid) => {
            // study exists?
            if (iid == null) {
              throw Error('Study does not exist');
            } else {
              // load history from uploaded data
              return contourModificationHistoryDataSet.loadDataAsync(uploadedHistoryData.StudyInstanceUID).then(() => {
                return contourModificationHistoryDataSet.setContourModifications(uploadedHistoryData.StudyInstanceUID, uploadedHistoryData.history);
              });
            }
          })
          .catch((error) => {
            setUploadedHistoryCreationFailed(true);
          })
          .finally(() => {
            contourModificationHistoryDataSet.delete();
            // we don't need the data anymore
            uploadedHistoryData = null;
          });
      }
    }
  };

  return (
    <label title="Upload data to the orthanc server" className="studyManagementButtonUpload">
      Upload Data
      <input type="file" accept=".dcm,.json,.zip" name="uploadedFiles" multiple hidden onChange={(event) => dataUploadHandler(event)} />
    </label>
  );
}
