import React, { useState, useEffect } from 'react';
import { confirmAlert } from 'react-confirm-alert';

import { getPatients, deletePatient } from '../API';
import { RenderTable } from './PatientDataTable';
import UploadDicomFileDialog from './UploadDicomFileDialog';
import { ModalDelete, ModalDeleting, ModalUploadActiveStudy, ModalStudyHasHistory, ModalUploadedHistoryFailed } from './Modals';
import { UploadButton } from './UploadButton';
import DataSet from './../../../data/DataSet.js';

export default function PatientDataMangement({ activeStudyInstanceUID, setStudy, activePatient, setActivePatient }) {
  const [patients, setPatients] = useState([]);
  const [isDeleting, setIsDeleting] = useState(false);
  const [patientDeletingID, setPatientDeletingID] = useState();

  // Variables for the upload process
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [filesToUpload, setFilesToUpload] = useState(0);

  // To show the upload modal
  const uploadDicomFileFunctions = {};

  const patientContentDialogFunction = {};

  const [confirmDelete, setConfirmDelete] = useState(false);
  const [uploadedHistoryCreationFailed, setUploadedHistoryCreationFailed] = useState(false);

  // Used to show an error modal
  const [isUploadingHistoryOfActiveStudy, setIsUploadingHistoryOfActiveStudy] = useState(false);
  const [studyAlreadyHasHistory, setStudyAlreadyHasHistory] = useState(false);

  // Load patients only when is rendered
  useEffect(() => {
    manageGetPatient();
  }, []);

  // This will load all the patients and show a modal in case of error
  const manageGetPatient = () => {
    return getPatients()
      .then((res) => {
        if (res.length != 0) {
          setPatients(res);
        } else {
          throw new Error('probando');
        }
      })
      .catch((error) => {
        console.log('No patients were found or error while loading patients');
        setPatients([]);
        return confirmAlert({
          title: 'Get patients',
          message:
            'Unable to read the list of patients or no patients are uploaded. Have you installed the Orthanc server? You can install the server by following the link on the home page',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
            },
          ],
        });
      });
  };

  // When we click on the deleteButton we will try to delete the patient of that row and after that reload the UI
  const handleDeletePatient = (patientID) => {
    setIsDeleting(false);
    setConfirmDelete(true);
    if (activePatient && patientID === activePatient.ID) {
      setActivePatient(null);
      setStudy(null);
      DataSet.getInstance().delete();
    }

    deletePatient(patientID).then(() => {
      manageGetPatient();
      setConfirmDelete(false);
    });
  };

  // When we click on the load button, we want to make that patient, the active patient
  const handleLoadClick = (patient) => {
    setActivePatient(patient);
    setStudy(null);
    DataSet.getInstance().delete();
  };

  // returns the text to be displayed in the load button depending on if the param is the active patient
  const buttonText = (patient) => {
    return activePatient?.MainDicomTags.PatientID === patient.MainDicomTags.PatientID ? 'loaded' : 'load';
  };

  return (
    <>
      <UploadButton
        setFilesToUpload={setFilesToUpload}
        setUploadedFiles={setUploadedFiles}
        uploadDicomFileFunctions={uploadDicomFileFunctions}
        activeStudyInstanceUID={activeStudyInstanceUID}
        setIsUploadingHistoryOfActiveStudy={setIsUploadingHistoryOfActiveStudy}
        setStudyAlreadyHasHistory={setStudyAlreadyHasHistory}
        uploadedFiles={uploadedFiles}
        setUploadedHistoryCreationFailed={setUploadedHistoryCreationFailed}
      />

      <RenderTable
        patients={patients}
        handleLoadClick={handleLoadClick}
        buttonText={buttonText}
        setIsDeleting={setIsDeleting}
        setPatientDeletingID={setPatientDeletingID}
        activePatient={activePatient}
      />

      <ModalDelete isDeleting={isDeleting} handleDeletePatient={handleDeletePatient} patientDeletingID={patientDeletingID} setIsDeleting={setIsDeleting} />

      <ModalDeleting confirmDelete={confirmDelete} />

      <ModalUploadActiveStudy
        isUploadingHistoryOfActiveStudy={isUploadingHistoryOfActiveStudy}
        setIsUploadingHistoryOfActiveStudy={setIsUploadingHistoryOfActiveStudy}
      />

      <ModalStudyHasHistory studyAlreadyHasHistory={studyAlreadyHasHistory} setStudyAlreadyHasHistory={setStudyAlreadyHasHistory} />

      <ModalUploadedHistoryFailed
        uploadedHistoryCreationFailed={uploadedHistoryCreationFailed}
        setUploadedHistoryCreationFailed={setUploadedHistoryCreationFailed}
      />

      <UploadDicomFileDialog
        uploadDicomFileFunctions={uploadDicomFileFunctions}
        uploadedList={uploadedFiles}
        numFilesToUpload={filesToUpload}
        onOk={() => manageGetPatient()}
      />
    </>
  );
}
