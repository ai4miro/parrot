import React from 'react';
import { Modal } from 'antd';
import { LinearProgress } from '@mui/material';
import DataSet from './../../../data/DataSet.js';
import { FiDelete } from 'react-icons/fi';
import { BiBookContent } from 'react-icons/bi';
import { SETTING_NAMES, MainSettings } from './../../../utils/MainSettings.js';
import StudyContentDialog from './StudyContentDialog.js';
import { confirmAlert } from 'react-confirm-alert';
// import 'react-confirm-alert/src/react-confirm-alert.css';
import './styles.css';

import { getStudiesOfPatient, getPatient, deleteStudy } from '../API.js';

class StudyManagementTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'studyManagement' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  openStudy(rowItem) {
    this.props.onClickOpenStudy(rowItem.MainDicomTags.StudyInstanceUID);
  }

  deleteStudy(rowItem, event) {
    this.props.onClickDeleteStudy(rowItem.MainDicomTags.StudyInstanceUID, event);
  }

  closeStudyContentDialog() {
    this.props.closeStudyContentDialog();
  }

  callOpenSelectedStudyContentDialog(item) {
    this.props.openStudyContentDialog(item.MainDicomTags.StudyInstanceUID);
  }

  handlerLoadButtonText(item) {
    if (this.props.activeStudy == item.MainDicomTags.StudyInstanceUID && !this.props.isLoading) {
      return 'Loaded';
    } else {
      return 'Load';
    }
  }

  renderStudyContentIconTD(item, style) {
    return (
      <td className="actionShowStudyContentTD" style={style}>
        <BiBookContent
          className={'showStudyContentRowIcon' + item.PatientMainDicomTags.PatientName}
          value={item.PatientMainDicomTags.PatientName}
          title="Show the study content"
          onClick={() => this.callOpenSelectedStudyContentDialog(item)}
          style={{
            fontSize: '25px',
            cursor: 'pointer',
          }}
        />
      </td>
    );
  }

  renderStudyManagementTBody() {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'center',
    };
    return (
      <tbody className="studyManagementTableTBody">
        {this.props.studies.map((item) => (
          <tr
            key={item.ID}
            className={this.props.activeStudy == item.MainDicomTags.StudyInstanceUID && !this.props.isLoading ? 'tableRowSelected' : 'noSelectedRow'}
          >
            {this.renderStudyContentIconTD(item, style)}
            <td
              className=" patientManagementTableBodyTD loadCell"
              style={{
                cursor: 'pointer',
              }}
              title="Loading study from the backend orthanc server. A study can be visualized and manipulated in the other two modules"
              onClick={() => this.openStudy(item)}
            >
              {this.handlerLoadButtonText(item)}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.PatientMainDicomTags.PatientName}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.PatientMainDicomTags.PatientID}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.PatientMainDicomTags.PatientBirthdate}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.PatientMainDicomTags.patientSex}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.MainDicomTags.StudyDate}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.MainDicomTags.StudyID}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.MainDicomTags.ReferringPhysicianName}
            </td>
            <td className="patientManagementTableBodyTD" value={item.MainDicomTags.StudyInstanceUID}>
              {item.modalities.join(', ')}
            </td>
            <td
              className="actionDeleteStudyTD deleteCell"
              onClick={(event) => {
                this.deleteStudy(item, event);
              }}
            >
              <FiDelete value={item.PatientMainDicomTags.PatientName} title="Delete the selected study from the application" />
            </td>
          </tr>
        ))}
      </tbody>
    );
  }

  render() {
    return (
      <div>
        <table
          className="studyManagmentTable"
          style={{
            border: 'sold 1px grey',
            fontSize: '14px',
            padding: '2px',
            textAlign: 'center',
            borderCollapse: 'inherit',
            width: '100%',
          }}
        >
          <thead>
            <tr className="studyManagementTableHeaderTR">
              {this.renderTableHeader('')}
              {this.renderTableHeader('')}
              {this.renderTableHeader('Patient')}
              {this.renderTableHeader('Patient ID')}
              {this.renderTableHeader('Birthday')}
              {this.renderTableHeader('Sex')}
              {this.renderTableHeader('Study Date')}
              {this.renderTableHeader('Study ID')}
              {this.renderTableHeader('Physician Name')}
              {this.renderTableHeader('Modalities')}
              {this.renderTableHeader('')}
            </tr>
          </thead>
          {this.renderStudyManagementTBody()}
        </table>
      </div>
    );
  }
}

export default class StudyDataManagement extends React.Component {
  constructor(props) {
    super(props);

    const settings = new MainSettings();
    const pacsURL = settings.get(SETTING_NAMES.PACS_URL);
    this.state = {
      activeStudyInstanceUID: this.props.activeStudyInstanceUID,
      studies: null,
      StudyInstanceUID: null,
      activePatient: this.props.activePatient,
      isLoading: false,
      isConfirmingDelete: false,
      isDeleting: false,
      studyMetaData: null,
      openedStudyContentUID: null,
    };

    this.dicomDataLoader = DataSet.getInstance().getDataLoader();
    this.dicomDataLoader.setPACSURL(pacsURL);
    this.dicomDataLoader.enable();
    this.loadStudyList();

    this.selectStudyFunctions = {};
    this.studyContentDialogFunction = {};
  }

  loadStudyList() {
    getStudiesOfPatient(this.state.activePatient)
      .then((studies) => {
        if (studies) {
          this.setState({
            studies: studies.slice(),
          });
        } else {
          this.setState({
            studies: null,
          });
        }
      })
      .catch((error) => {
        // In case there is an error, we want to make sure we reload the UI with no studies
        console.error(error);
        this.setState({
          studies: null,
        });
        return confirmAlert({
          title: 'Get studies',
          message: 'Unable to read the list of studies. Have you loaded a patient? You can load one on the Patient Management tab',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {
                this.studyContentDialogFunction.setVisible(false);
              },
            },
          ],
        });
      });
  }

  handleClickOpenStudy(StudyInstanceUID) {
    this.setState({
      StudyInstanceUID: StudyInstanceUID,
      isLoading: true,
    });
    DataSet.getInstance()
      .loadStudyAsync(StudyInstanceUID)
      .finally(() => {
        this.setState({
          isLoading: false,
        });
      });
    this.props.setStudy(StudyInstanceUID);
    this.setState({
      activeStudyInstanceUID: StudyInstanceUID,
    });
  }

  handleClickDeleteStudy(siuid) {
    this.setState({
      isConfirmingDelete: true,
      studyIUIDDelete: siuid,
    });
  }

  handleConfirmedDeleteStudy() {
    const siuid = this.state.studyIUIDDelete;
    this.setState({
      isDeleting: true,
    });
    // unload the study
    if (siuid == this.state.activeStudyInstanceUID) {
      this.setState({
        StudyInstanceUID: null,
        activeStudyInstanceUID: null,
      });

      DataSet.getInstance()
        .delete()
        .then(() => {
          this.dicomDataLoader.enable();
        });
    }

    // delete in server
    deleteStudy(siuid).finally(() => {
      this.setState({
        isDeleting: false,
      });

      console.log(this.state.activePatient.Studies.length);
      // If we are deleting the last study of a patient, it will return null so no studies will be displayed
      getPatient(this.state.activePatient.ID).then((patient) => {
        this.setState({ activePatient: patient });
        this.props.setActivePatient(patient);
        this.loadStudyList();
      });
    });
  }

  catchLoadStudyMetaDataError(error, errorType) {
    return confirmAlert({
      title: 'Show the study metadata and downloand features',
      message: 'Loading study content failed: ' + errorType,
      closeOnEscape: false,
      closeOnClickOutside: false,
      buttons: [
        {
          label: 'OK',
          onClick: () => {
            this.studyContentDialogFunction.setVisible(false);
          },
        },
      ],
    });
  }

  async openStudyContentDialog(studyUID) {
    this.setState({
      studyMetaData: null,
      openedStudyContentUID: null,
    });
    this.studyContentDialogFunction.setVisible(true);

    const meta = await this.dicomDataLoader.getStudyMetaData(studyUID).catch((error) => {
      this.catchLoadStudyMetaDataError(error, 'Meta data');
    });
    let downloadInfoData = [];
    const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json().catch((error) => {
      this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
    });

    for (let t = 0; t < listOfStudyIDs.length; t++) {
      const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[t], { method: 'GET' })).json().catch((error) => {
        this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
      });

      if (studyInfo.MainDicomTags.StudyInstanceUID == studyUID) {
        const series = studyInfo.Series;
        for (let s = 0; s < series.length; s++) {
          const instances = await (await fetch('/getInstances/' + series[s], { method: 'GET' })).json().catch((error) => {
            this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
          });

          const dicomTages = instances.MainDicomTags;
          if (dicomTages.Modality == 'RTDOSE' || dicomTages.Modality == 'RTPLAN' || dicomTages.Modality == 'RTSTRUCT' || dicomTages.Modality == 'REG') {
            const data = { seriesIUID: dicomTages.SeriesInstanceUID, modality: dicomTages.Modality, instanceID: instances.Instances[0] };
            downloadInfoData.push(data);
          }
        }
      }
    }

    this.setState({
      studyMetaData: { metaData: meta, downloadInfoData: downloadInfoData },
      openedStudyContentUID: studyUID,
    });
  }

  renderStudyManagementTable(studies) {
    if (studies) {
      return (
        <StudyManagementTable
          className="studyManagementTable"
          studies={studies}
          onClickOpenStudy={(StudyInstanceUID) => this.handleClickOpenStudy(StudyInstanceUID)}
          onClickDeleteStudy={(StudyInstanceUID, event) => this.handleClickDeleteStudy(StudyInstanceUID, event)}
          selectStudyFunctions={this.selectStudyFunctions}
          activeStudy={this.state.activeStudyInstanceUID}
          openStudyContentDialog={(id) => this.openStudyContentDialog(id)}
          isLoading={this.state.isLoading}
        ></StudyManagementTable>
      );
    }
  }

  render() {
    const patientName = this.state.activePatient !== null ? this.state.activePatient.MainDicomTags.PatientName : '';
    return (
      <div>
        {this.renderStudyManagementTable(this.state.studies)}
        <Modal className="loadingPreviewDialog" closable={false} visible={this.state.isLoading} footer={null}>
          <h3 className="loadingStudyPatientlabel">Loading study of patient {patientName} ... </h3>
          <LinearProgress className="linearProgress"></LinearProgress>
        </Modal>
        <Modal
          className="confirmDeleteStudyDialog"
          closable={false}
          open={this.state.isConfirmingDelete}
          onCancel={() =>
            this.setState({
              isConfirmingDelete: false,
            })
          }
          onOk={() => {
            this.setState({ isConfirmingDelete: false });
            this.handleConfirmedDeleteStudy();
          }}
          style={{ width: 'fit-content' }}
        >
          <h3 className="confirmDeleteStudylabel" style={{ color: 'yellow', textAlign: 'center', fontSize: '25px' }}>
            Do you really want to delete the study?
          </h3>
        </Modal>
        <Modal className="deletingDialog" closable={false} open={this.state.isDeleting} footer={null} style={{ width: 'fit-content' }}>
          <h3 className="deletingStudyPatientlabel" style={{ textAlign: 'center', color: 'red', fontSize: '25px' }}>
            Deleting Study...
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryFileActiveStudyDialog"
          closable={false}
          open={this.state.isUploadingHistoryOfActiveStudy}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ isUploadingHistoryOfActiveStudy: false });
          }}
        >
          <h3 className="confirmUploadHistoryActiveStudylabel">
            The modification history that you are trying to upload belongs to the currently active study and has therefore been ignored. Uploading it would
            result in inconsistent modifications. Please reload the webpage to unload the currently active study and try again.
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryAlreadyModifiedDialog"
          closable={false}
          open={this.state.studyAlreadyHasHistory}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ studyAlreadyHasHistory: false });
          }}
        >
          <h3 className="confirmUploadHistoryAlreadyModifiedLabel">
            The modification history that you are trying to upload belongs to an existing study that already has modifications. To replace the modification
            history, either delete the existing modifications or delete and reupload the entire study.
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryNoProcessedDialog"
          closable={false}
          open={this.state.uploadedHistoryCreationFailed}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ uploadedHistoryCreationFailed: false });
          }}
        >
          <h3 className="confirmUploadHistoryNoProcessedLabel">
            The modification history that you have uploaded could not be processed. Please verify that it belongs to one of the existing studies.
          </h3>
        </Modal>
        <StudyContentDialog
          className="studyContentDialog"
          studyContentDialogFunction={this.studyContentDialogFunction}
          studyMetaData={this.state.studyMetaData}
          studies={this.state.studies}
          openedStudyContentUID={this.state.openedStudyContentUID}
          activeStudy={this.state.activeStudyInstanceUID}
          onReloadActiveStudy={() => this.handleClickOpenStudy(this.state.activeStudyInstanceUID)}
        ></StudyContentDialog>
      </div>
    );
  }
}
