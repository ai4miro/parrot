// Returns the patient with the ID given in the params
// Right now is being used to reload the patient when you delete a study
export async function getPatient(patientID) {
  console.log('searching patient with id  -> ' + patientID);

  let url = `getpatient/${patientID}`;

  return await fetch(url).then((res) => {
    if (res.ok) {
      return res.json();
    } else {
      return null;
    }
  });
}

// Returns the patients storaged on the orthanc server
export async function getPatients() {
  console.log('search for patients');

  let url = '/getpatient/';
  const patientsID = await fetch(url).then((res) => res.json());

  const patientPromises = patientsID.map(async (id) => {
    const patientUrl = `${url}/${id}`;
    const response = await fetch(patientUrl);
    return response.json();
  });

  const patients = await Promise.all(patientPromises);

  return patients;
}

// Returns the studies of the patient given in the params
export async function getStudiesOfPatient(patient) {
  console.log('search for studies of patient -> ' + patient.MainDicomTags.PatientName);
  const studiesID = patient.Studies;

  const studiesPromises = studiesID.map(async (id) => {
    const studyUrl = `/getstudy/${id}`;
    const response = await fetch(studyUrl);
    return response.json();
  });

  const studies = await Promise.all(studiesPromises);
  for (let i = 0; i < studies.length; i++) {
    const study = studies[i];
    console.log('Lookig for modalities of the study -> ' + study.ID);
    const modalities = await getModalities(study.Series);
    studies[i].modalities = modalities;
  }
  return studies;
}

// Delete the patient with ID given in the params
export async function deletePatient(patientID) {
  console.log('delete patient with id -> ' + patientID);
  const patientsID = await (await fetch('/getpatient/', { method: 'GET' })).json();

  if (patientsID.includes(patientID)) {
    return await fetch(`/deletepatient/${patientID}`, { method: 'DELETE' })
      .then((res) => res.json())
      .catch((err) => {
        throw Error(err.message);
      });
  }
}

// Delete the study with ID given in the params
export async function deleteStudy(siuid) {
  console.log('delete study with id -> ' + siuid);

  const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json();
  for (var i = 0; i < listOfStudyIDs.length; i++) {
    const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[i], { method: 'GET' })).json();
    if (studyInfo.MainDicomTags.StudyInstanceUID == siuid) {
      await fetch('/deletestudy/' + listOfStudyIDs[i], { method: 'DELETE' });
      break;
    }
  }
}

// Return de modalities of the series of a study
export async function getModalities(seriesId) {
  let modalities = [];
  for (let i = 0; i < seriesId.length; i++) {
    const serie = await (await fetch('/getInstances/' + seriesId[i], { method: 'GET' })).json();
    const modality = serie.MainDicomTags.Modality;
    if (!modalities.includes(modality)) {
      modalities.push(modality);
    }
  }
  return modalities;
}
