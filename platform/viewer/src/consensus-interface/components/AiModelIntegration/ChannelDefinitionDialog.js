import React, { useMemo } from 'react';
import { Button, Modal, Space } from 'antd';
import { CgAddR } from 'react-icons/cg';
import { FiArrowDown, FiArrowUp, FiDelete } from 'react-icons/fi';
import { useTable } from 'react-table';
import PropTypes from 'prop-types';
import './styles.css';

export const ChannelDefineTable = ({ channels, onDefineChannelChanged }) => {
  const headerStyle = {
    border: 'solid 1px black',
    textAlign: 'center',
    fontSize: '14px',
    padding: '2px',
    background: 'grey',
    color: 'white',
  };

  const tdStyle = {
    padding: '1px',
    border: 'solid 1px grey',
    fontSize: '14px',
    textAlign: 'center',
    cursor: 'default',
    color: 'white',
  };

  const columns = useMemo(
    () => [
      {
        Header: 'Input channel',
        accessor: 'channelId',
        Cell: (cell) => {
          return <div style={{ width: '35px', textAlign: 'center' }}>{parseInt(cell?.row?.id) + 1}</div>;
        },
      },
      {
        Header: 'Name',
        accessor: 'contourName',
        Cell: (cell) => {
          return (
            <input
              className="contourDefinitionInput"
              id={cell.row.id}
              defaultValue={cell.row.original}
              placeholder="Contour name"
              type="text"
              size="60"
              required={true}
              style={{ color: 'black' }}
              onChange={(event) => {
                if (event.target.value !== cell?.row?.original) {
                  channels[cell.row.id] = event.target.value;
                }
              }}
            />
          );
        },
      },
    ],
    [onDefineChannelChanged]
  );

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data: channels,
    },
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        ...columns,
        {
          id: 'up',
          cell: ({ row }) => <div />,
        },
        {
          id: 'down',
          cell: ({ row }) => <div />,
        },
        {
          id: 'delete',
          cell: ({ row }) => <div />,
        },
      ]);
    }
  );

  const onAddNewRow = () => {
    const newChannel = channels.concat(['']);
    onDefineChannelChanged(newChannel);
  };

  const onDeleteRow = (id) => {
    const newChannel = channels.toSpliced(id, 1);
    onDefineChannelChanged(newChannel);
  };

  const onMoveRowUp = (id) => {
    if (id > 0) {
      const newChannel = channels
        .slice(0, id - 1)
        .concat([channels[id], channels[id - 1]])
        .concat(channels.slice(id + 1));
      onDefineChannelChanged(newChannel);
    }
  };

  const onMoveRowDown = (id) => {
    if (id < channels.length - 1) {
      const newChannels = channels
        .slice(0, id)
        .concat([channels[id + 1], channels[id]])
        .concat(channels.slice(id + 2));
      onDefineChannelChanged(newChannels);
    }
  };

  return (
    <table className="contourDefinitionTable" {...getTableProps()} style={{ width: '100%' }}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr className="contourDefintionTabHeaderTR" {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th style={headerStyle} className="contourDefinitionHeaderTH" {...column.getHeaderProps()}>
                {column.id === 'delete' ? (
                  <td>{<CgAddR className="addNewContourRow" title="Add a new contour" onClick={(event) => onAddNewRow()} />}</td>
                ) : (
                  column.render('Header')
                )}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody className="contourDefintionTableTBody" {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} className="contourDefinitionTableTR">
              {row.cells.map((cell) => {
                return (
                  <td {...cell.getCellProps()} style={tdStyle}>
                    {(function () {
                      switch (cell.column.id) {
                        case 'delete':
                          return (
                            <FiDelete
                              className="deleteContourRowIcon"
                              style={{
                                fontSize: '20px',
                                color: 'red',
                                width: '20px',
                                cursor: 'pointer',
                              }}
                              onClick={(event) => onDeleteRow(parseInt(row.id))}
                            />
                          );
                        case 'up':
                          return (
                            <FiArrowUp
                              className="moveupContourRowIcon"
                              style={{
                                fontSize: '20px',
                                color: 'green',
                                width: '20px',
                                cursor: 'pointer',
                              }}
                              onClick={(event) => onMoveRowUp(parseInt(row.id))}
                            />
                          );
                        case 'down':
                          return (
                            <FiArrowDown
                              className="movedownContourRowIcon"
                              style={{
                                fontSize: '20px',
                                color: 'green',
                                width: '20px',
                                cursor: 'pointer',
                              }}
                              onClick={(event) => onMoveRowDown(parseInt(row.id))}
                            />
                          );
                        default:
                          return cell.render('Cell');
                      }
                    })()}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default class ChannelDefinitionDialog extends React.Component {
  constructor(props) {
    super(props);
    ChannelDefinitionDialog.PropTypes = {
      contourDefinitionDialogFunctions: PropTypes.object,
      tarChannels: PropTypes.object,
      oarChannels: PropTypes.object,
      onDefineChannelChanged: PropTypes.func,
    };

    props.contourDefinitionDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      errorMessage: '',
      tarChannels: [...this.props.tarChannels],
      oarChannels: [...this.props.oarChannels],
    };
  }

  componentDidUpdate(prevProps) {
    this.props.contourDefinitionDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    if (prevProps.tarChannels != this.props.tarChannels || prevProps.oarChannels != this.props.oarChannels) {
      this.setState({
        tarChannels: [...this.props.tarChannels],
        oarChannels: [...this.props.oarChannels],
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
      errorMessage: '',
    });
  }

  handleOk() {
    // trim all contour names
    const tarChannels = this.state.tarChannels.map((c) => c.trim());
    const oarChannels = this.state.oarChannels.map((c) => c.trim());
    // check if there is no empty contour name
    if (tarChannels.includes('') || oarChannels.includes('')) {
      this.setState({ errorMessage: 'One or more contour names are empty' });
      return;
    }
    // everything okay
    this.props.onDefineChannelChanged(tarChannels, oarChannels);
    this.setVisible(false);
  }

  handleCancel() {
    this.setState({
      tarChannels: [...this.props.tarChannels],
      oarChannels: [...this.props.oarChannels],
    });
    this.setVisible(false);
  }

  render() {
    return (
      <Modal
        className="contourDefinitionModal"
        open={this.state.visible}
        closable={false}
        destroyOnClose={true}
        onOk={() => this.handleOk()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" onClick={() => this.handleOk()}>
            Ok
          </Button>,
        ]}
        style={{ zIndex: '1001' }}
        cancelButtonProps={{ style: { display: 'none' } }}
      >
        {this.state.errorMessage ? <h3 style={{ color: 'red' }}>{this.state.errorMessage}</h3> : null}
        <Space direction="vertical">
          <div style={{ textAlign: 'center', backgroundColor: 'grey', fontWeight: 'bold' }}>Target volumns</div>
          <div className="targetVolumnsDiv" style={{ overflow: 'auto', height: 'fit-content', maxHeight: '210px' }}>
            <ChannelDefineTable
              channels={this.state.tarChannels}
              onDefineChannelChanged={(newChannels) => {
                this.setState({ tarChannels: newChannels });
              }}
            />
          </div>
          <div style={{ textAlign: 'center', backgroundColor: 'grey', fontWeight: 'bold' }}>Organs at risk</div>
          <div className="organsAtRiskDiv" style={{ overflow: 'auto', height: 'fit-content', maxHeight: '210px' }}>
            <ChannelDefineTable
              channels={this.state.oarChannels}
              onDefineChannelChanged={(newChannels) => {
                this.setState({ oarChannels: newChannels });
              }}
            />
          </div>
        </Space>
      </Modal>
    );
  }
}
