import DICOMWeb from '../utils/DICOMWeb/'; //TODO: use dcmjs instead

// https://dicomweb-client.readthedocs.io/en/latest/introduction.html
import { api } from '../utils/dicomweb-client/dicomweb-client';
import dcmjs from 'dcmjs';

export default class DicomDataLoader {
  static instance = null;
  pacsURL = null;
  client = null;
  enaled = true;
  runningNb = 0;

  enable() {
    this.enaled = true;
  }

  async disable() {
    this.enaled = false;

    await this.waitUntilIsDisabled();
  }

  async waitUntilIsDisabled() {
    const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

    while (this.runningNb) {
      await sleep(50);
    }
    if (!this.enaled)
      // if was intentionnaly disable, wait 10 ms more to be sure return statement occured.
      sleep(10);
  }

  setPACSURL(pacsURL) {
    if (pacsURL == this.pacsURL) return;

    this.pacsURL = pacsURL;
    this.client = new api.DICOMwebClient({ url: pacsURL });
  }

  static getInstance() {
    if (!DicomDataLoader.instance) DicomDataLoader.instance = new DicomDataLoader();

    return this.instance;
  }

  async getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled) return null;

    this.runningNb++;

    const options = {
      studyInstanceUID: StudyInstanceUID,
      seriesInstanceUID: SeriesInstanceUID,
      sopInstanceUID: SOPInstanceUID,
      // new orthanc version uses explicit transfer syntax by default, and if you have uploaded
      // a dicom file with implicit transfer syntax, the contourdata will have UN (Unknown)
      // as value representation and then SegDataSet will not be able to load the contour.
      mediaTypes: [{ mediaType: 'application/dicom', transferSyntaxUID: '1.2.840.10008.1.2' }],
    };

    const data = await this.client.retrieveInstance(options);

    const dicomData = dcmjs.data.DicomMessage.readFile(data);

    this.runningNb--;
    return dcmjs.data.DicomMetaDictionary.naturalizeDataset(dicomData.dict);
  }

  async getInstanceData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, seriesMetaData) {
    if (!this.enaled) return null;

    this.runningNb++;

    const options = {
      studyInstanceUID: StudyInstanceUID,
      seriesInstanceUID: SeriesInstanceUID,
      sopInstanceUID: SOPInstanceUID,
    };

    if (!seriesMetaData) seriesMetaData = await this.getSeriesMetaData(url, StudyInstanceUID, SeriesInstanceUID); // We could retrieve them directly from client.retrieveInstance

    const data = await this.client.retrieveInstance(options);
    const data16 = new Int16Array(data);

    let newData = data16.slice(data16.length - seriesMetaData[0].Rows * seriesMetaData[0].Columns, data16.length);

    if (seriesMetaData[0].hasOwnProperty('RescaleIntercept') && seriesMetaData[0].RescaleSlope) {
      // Old code:
      // newData = newData.map(elem => elem*seriesMetaData[0].RescaleSlope + seriesMetaData[0].RescaleIntercept);
      const slope = seriesMetaData[0].RescaleSlope;
      const intercept = seriesMetaData[0].RescaleIntercept;
      for (let i = 0; i < newData.length; i++) {
        newData[i] = newData[i] * slope + intercept;
      }
    }

    this.runningNb--;

    return newData;
  }

  async getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc) {
    this.runningNb++;

    let data = new Int16Array(seriesMetaData[0].Rows * seriesMetaData[0].Columns * seriesMetaData.length);

    // metadata already sorted by getSeriesMetaData
    /*
    let ind = 0;
    for (let i=0; i<seriesMetaData.length; i++) {
      if (!this.enaled) {
        this.runningNb--;
        
        return null;
      }
      
      console.log('Loading instance ' + (i+1) + '/' + seriesMetaData.length);
      
      let instanceMetada = seriesMetaData[i];
      let instanceData = await this.getInstanceData(instanceMetada.StudyInstanceUID, instanceMetada.SeriesInstanceUID, instanceMetada.SOPInstanceUID, seriesMetaData);
      data.set(instanceData, ind);
      ind = ind+instanceData.length;
      
      progressFunc((i+1)/seriesMetaData.length);
    }
    */

    var numFinished = 0;
    var promises = seriesMetaData.map((md, i) =>
      this.getInstanceData(md.StudyInstanceUID, md.SeriesInstanceUID, md.SOPInstanceUID, seriesMetaData).then((instanceData) => {
        data.set(instanceData, instanceData.length * i);
        numFinished++;
        // console.log('Loaded instance ' + (i+1) + '/' + seriesMetaData.length)
        progressFunc(numFinished / seriesMetaData.length);
      })
    );
    await Promise.all(promises);

    this.runningNb--;
    return data;
  }

  async getSEGData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled) return null;

    this.runningNb++;

    const res = await this.getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID);

    this.runningNb--;
    return res;
  }

  // Todo: history data from server
  async getContourModificationHistoryData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enable) {
      return { imageData: null, metaData: null };
    }
    this.runningNb++;

    const seriesMetaData = null;
    const imageData = null;

    this.runningNb--;
    return { imageData, metaData: seriesMetaData };
  }

  async getRTDOSEData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    return this.getSEGData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID);
  }

  async getSeriesData(StudyInstanceUID, SeriesInstanceUID, progressFunc) {
    if (!this.enaled) return { data: null, metaData: null };

    this.runningNb++;

    const seriesMetaData = await this.getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID);
    const data = await this.getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc);

    this.runningNb--;
    return { data, metaData: seriesMetaData };
  }

  async getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID) {
    if (!this.enaled) return null;

    this.runningNb++;

    const options = {
      studyInstanceUID: StudyInstanceUID,
      seriesInstanceUID: SeriesInstanceUID,
    };

    const seriesMetaData = await this.client.retrieveSeriesMetadata(options);
    let res = resultDataToStudies(seriesMetaData, 'getSeriesMetaData').slice();
    //res.sort((metadata1, metadata2) => metadata1.InstanceNumber > metadata2.InstanceNumber ? 1 : -1); // We have to return a real number and not a boolean in chrome!
    //res.sort((metadata1, metadata2) => (metadata1.SliceLocation < metadata2.SliceLocation ? 1 : -1)); // We have to return a real number and not a boolean in chrome!
    res.sort((metadata1, metadata2) => (metadata1.ImagePositionPatient[2] < metadata2.ImagePositionPatient[2] ? 1 : -1));
    this.runningNb--;
    return res;
  }

  async getStudies() {
    if (!this.enaled) return null;

    this.runningNb++;

    const resultData = await this.client.searchForStudies();
    const res = resultDataToStudies(resultData, 'getStudies');

    this.runningNb--;
    return res;
  }

  async getStudyMetaData(StudyInstanceUID) {
    if (!this.enaled) return null;

    this.runningNb++;

    const options = { studyInstanceUID: StudyInstanceUID };

    const metaData = await this.client.retrieveStudyMetadata(options);

    const res = resultDataToStudies(metaData, 'getStudyMetaData');

    this.runningNb--;
    return res;
  }

  async getStudyMetaDataFast(StudyInstanceUID) {
    if (!this.enaled) return null;

    this.runningNb++;

    const seriesRaw = await this.client.searchForSeries({ studyInstanceUID: StudyInstanceUID });
    const series = resultDataToStudies(seriesRaw, 'getStudies');
    var instancesRaw = [];
    for (var i = 0; i < series.length; i++) {
      const md = await this.client.searchForInstances({
        studyInstanceUID: StudyInstanceUID,
        seriesInstanceUID: series[i].SeriesInstanceUID,
      });
      instancesRaw = instancesRaw.concat(md);
    }
    const res = resultDataToStudies(instancesRaw, 'getStudies');

    this.runningNb--;
    return res;
  }

  sendSEGData(metaData) {
    console.log('Not implemented yet - Cannot send:');
    console.log(metaData);
  }
}

function resultDataToStudies(resultData, source) {
  const studies = [];

  if (!resultData || !resultData.length) return;

  resultData.forEach((study) => {
    let PixelSpacing = '0.0';
    let ImagePositionPatient = '0.0';
    let SliceThickness = '';
    let ImageOrientationPatient = '';
    let RescaleIntercept = '0.0';
    let RescaleSlope = '1.0';
    let InstanceCreationDate = '';
    let InstanceNumber = '';
    let SliceLocation = '0.0';

    if (study['00280030']) PixelSpacing = study['00280030'].Value.slice();
    if (study['00200032']) ImagePositionPatient = study['00200032'].Value.slice();
    if (study['00200037']) ImageOrientationPatient = study['00200037'].Value.slice();
    if (study['00180050'] && study['00180050'].Value && study['00180050'].Value.length > 0) SliceThickness = study['00180050'].Value[0];
    if (study['00281052'] && study['00281052'].Value && study['00281052'].Value.length > 0) RescaleIntercept = study['00281052'].Value[0];
    if (study['00281053'] && study['00281053'].Value && study['00281053'].Value.length > 0) RescaleSlope = study['00281053'].Value[0];
    if (study['00080012'] && study['00080012'].Value && study['00080012'].Value.length > 0) InstanceCreationDate = DICOMWeb.getString(study['00080012']);
    if (study['00200013'] && study['00200013'].Value && study['00200013'].Value.length > 0) InstanceNumber = study['00200013'].Value[0];
    if (study['00201041'] && study['00201041'].Value && study['00201041'].Value.length > 0) SliceLocation = study['00201041'].Value[0];

    studies.push({
      StudyInstanceUID: DICOMWeb.getString(study['0020000D']),
      // 00080005 = SpecificCharacterSet
      StudyDate: study['00080020'] ? DICOMWeb.getString(study['00080020']) : '',
      StudyTime: study['00080030'] ? DICOMWeb.getString(study['00080030']) : '',
      AccessionNumber: study['00080050'] ? DICOMWeb.getString(study['00080050']) : '',
      ReferringPhysicianName: study['00080090'] ? DICOMWeb.getName(study['00080090']) : '',
      // 00081190 = URL
      PatientName: study['00100010'] ? DICOMWeb.getName(study['00100010']) : '',
      PatientID: DICOMWeb.getString(study['00100020']) ? DICOMWeb.getString(study['00100020']) : 'Unknown',
      PatientBirthdate: DICOMWeb.getString(study['00100030']),
      patientSex: study['00100030'] ? DICOMWeb.getString(study['00100040']) : 'Unknown',
      studyId: study['00200010'] ? DICOMWeb.getString(study['00200010']) : '',
      numberOfStudyRelatedSeries: study['00201206'] ? DICOMWeb.getString(study['00201206']) : '',
      numberOfStudyRelatedInstances: study['00201208'] ? DICOMWeb.getString(study['00201208']) : '',
      StudyDescription: study['00081030'] ? DICOMWeb.getString(study['00081030']) : '',
      SeriesDescription: study['0008103E'] ? DICOMWeb.getString(study['0008103E']) : '',
      // Modality: DICOMWeb.getString(study['00080060']),
      // ModalitiesInStudy: DICOMWeb.getString(study['00080061']),
      modalities: DICOMWeb.getString(DICOMWeb.getModalities(study['00080060'], study['00080061'])),
      SeriesInstanceUID: DICOMWeb.getString(study['0020000E']),
      PatientPosition: study['00185100'] ? DICOMWeb.getString(study['00185100']) : '',
      Manufacturer: study['00080070'] ? DICOMWeb.getString(study['00080070']) : '',
      SOPInstanceUID: study['00080018'] ? DICOMWeb.getString(study['00080018']) : '',
      Rows: study['00280010'] ? study['00280010'].Value[0] : null,
      Columns: study['00280011'] ? study['00280011'].Value[0] : null,
      BitsAllocated: study['00280100'] ? DICOMWeb.getString(study['00280100']) : '0',
      QueryRetrieveLevel: study['00080052'] ? DICOMWeb.getString(study['00080052']) : null,
      RetrieveAETitle: study['00080054'] ? DICOMWeb.getString(study['00080054']) : null,
      DeviceSerialNumber: study['00181000'] ? DICOMWeb.getString(study['00181000']) : null,

      InstanceNumber,
      PixelSpacing,
      ImagePositionPatient,
      ImageOrientationPatient,
      SliceThickness,
      RescaleIntercept,
      RescaleSlope,
      InstanceCreationDate,
      SliceLocation,
    });
  });

  return studies;
}
