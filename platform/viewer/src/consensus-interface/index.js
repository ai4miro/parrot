import React, { Component } from 'react';
import ReactDOM from 'react-dom/client';

import HomePage from './consensus-interface/components/HomePage.js';
import './consensus-interface/theme-tide.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <HomePage />
  </React.StrictMode>
);
